#include "BLEControl.h"
#include "BLEDevice.h"
#include "BLEServer.h"
#include "esp_bt.h"
#include "esp_gatts_api.h"

namespace ble {
  BLEServer* pServer;

  void init(
    SafeString &name,
    void (*onConnectFunc)(BLEServer* pServer),
    void (*onDisconnectFunc)(BLEServer* pServer),
    void (*onMtuChangedFunc)(BLEServer* pServer, esp_ble_gatts_cb_param_t* param)
  ) {
    BLEDevice::init(name.c_str());
    //BLEDevice::setPower(ESP_PWR_LVL_P21, ESP_BLE_PWR_TYPE_ADV);
    //BLEDevice::setPower(ESP_PWR_LVL_P21, ESP_BLE_PWR_TYPE_DEFAULT);
    BLEDevice::setPower(ESP_PWR_LVL_N0, ESP_BLE_PWR_TYPE_ADV); // POWERTEST
    BLEDevice::setPower(ESP_PWR_LVL_N0, ESP_BLE_PWR_TYPE_DEFAULT); // POWERTEST
    pServer = BLEDevice::createServer();
    pServer->setCallbacks(
      new BLEServerCbks(onConnectFunc, onDisconnectFunc, onMtuChangedFunc)
    );
  }
  
  void start() {
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    pAdvertising->setScanResponse(true);
    pAdvertising->setMinPreferred(0x06);  // functions that help with iPhone connections issue
    pAdvertising->setMinPreferred(0x12);
    BLEDevice::startAdvertising();
  }
  
  void stop() {
    BLEDevice::stopAdvertising();
    BLEDevice::deinit(true);
  }
  
  BLEService* createService(BLEUUID uuid, uint32_t numHandles) {
    BLEService *pService = pServer->createService(uuid, numHandles);
    return pService;
  }
  
  void startService(BLEService *pService) {
    pService->start();
    BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
    BLEUUID uuid = pService->getUUID();
    pAdvertising->addServiceUUID(uuid);
  }
}
