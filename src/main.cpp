#include <Arduino.h>
#include <ESP32_Mcu.h>
#include <Wire.h>
#include <SafeString.h>
#include <HTTPClient.h>
#include <Update.h>
#include <esp_sntp.h>
#include <time.h>

#include "Adafruit_SHT4x.h"
#include "BLECharacteristic.h"
#include "MC34x9.h"

#include "HttpsOTAUpdatePatched.h"
#include "BLEConfig.h"
#include "Protocol.h"
#include "Storage.h"
#include "TaskFunctions.h"
#include "Utils.h"
#include "Sensors.h"
#include "Reading.h"

#include "Battery.h"
#include "Hardware.h"
#include "LED.h"
#include "LoRa.h"
#include "MP2667.h"
#include "SDControl.h"
#include "BLEControl.h"
#include "RTC.h"
#include "WiFiControl.h"
#include "WiFiGeneric.h"

constexpr uint8_t  CONFIG_IDLE_TIME_S = 60;
constexpr uint32_t BUTTON_DURATION_TO_SLEEP_MS = 3000;
constexpr uint16_t BLE_UPDATE_PROGRESS_THROTTLE_MS = 500;
constexpr uint16_t SENSOR_SEND_INTERVAL = 1000;
constexpr uint16_t WIFI_CONNECT_TIMEOUT_MS = 10000;
constexpr uint16_t NTP_TIMEOUT_MS = 5000;
constexpr uint16_t NTP_UPDATE_INTERVAL_DAYS = 1;
constexpr uint8_t  PRECYCLE_DURATION_S = 15;
constexpr uint32_t MIN_RX_TIMEOUT_MS = 3000;

uint16_t HTTP_TIMEOUT_MS = 5000;
createSafeString(FACTORY_BIN_PATH, 12, "/factory.bin");
createSafeString(CACERT_PATH, 24, "/.well-known/ca_cert.pem");
createSafeString(TEST_PATH, 13, "/gwtest");
createSafeString(READING_PATH, 8, "/reading");
createSafeString(UPDATE_PATH_PREFIX, 9, "/gateway/");

MP2667 mp2667;
Adafruit_SHT4x sht40;
MC34X9 mc3479;

uint32_t lastUpdateProgressMillis = millis();

Reading reading;

hw_timer_t *powerOffTimer = NULL;

uint8_t bootProgress = 0;

RTC_DATA_ATTR uint64_t timeWakeUp = 0;
RTC_DATA_ATTR uint64_t timeNTPUpdated = 0;
RTC_DATA_ATTR int32_t timeAdjustSeconds = 0;
bool timeAdjusted = false;
uint64_t timeFirstPacketSent = 0;
uint64_t timeLastPacketSent = 0;

uint32_t millisButtonStartPress = 0;
uint32_t millisButtonPressed = 0;

uint8_t numReadingsReceived = 0;

// Global values
uint8_t gMacBytes[6];
createSafeString(gMacAscii, 12);
bool gIsPairing = false;
createSafeString(gToken, 36);
uint8_t gInterval;
uint8_t wakeupInterval;
createSafeString(gHTTPUserAgent, 32); // "hiivegateway/12.00 889900aabbcc"
createSafeString(gFirmwareVersion, 16, FIRMWARE_VERSION);
createSafeString(gFirmwareUpdateVersion, 32);
createSafeString(gWifiSsid, 32);
createSafeString(gWifiPassword, 64);
createSafeString(gAPIDomain, 64);
createSafeString(gStaticDomain, 64);
uint8_t gBattery;
#if defined(DEBUG) && DEBUG > 0
createSafeString(gDevAuthUser, 32, HTTP_AUTH_USER);
createSafeString(gDevAuthPassword, 64, HTTP_AUTH_PASSWORD);
#endif
createSafeString(gCACert, 2048);
int32_t gBytesReceivedReadingTransfer = -1;
bool gShippingModeDisabled;
TestResult gTestResult;
uint8_t gNumStartCyclePacketsSent = 0;
uint16_t gMtu = 23;
bool gBleConnected;

FunctionState functionState;
ConfigState configState;
GatewayState gatewayState;

MessageCode lastSentMessageCode;
void setLastSentMessage(MessageCode messageCode) {
  lastSentMessageCode = messageCode;
}

esp_sleep_wakeup_cause_t wakeupCause;

void initialize();
void initializeAfterUpdate();
void batteryGuard();

void resetStorageAndHardware();
void systemReset();
void updateFirmware(SafeString &firmwareUpdateVersion);

void startPowerOffTimer();
void stopPowerOffTimer();
void onPowerOffTimer();

void startSleep(bool buttonTriggered = false);
void setRTCAlarm(bool startNewCycle = false);

void pairBegin();
void pairStartCycle();

void onConfigTxDone();
void onConfigTxTimeout();
void onConfigRxDone(uint8_t *payload, uint16_t size, int16_t rssi);
void onConfigRxTimeout();
void onConfigRxError();

void onGatewayTxDone();
void onGatewayTxTimeout();
void onGatewayRxDone(uint8_t *payload, uint16_t size, int16_t rssi);
void onGatewayRxTimeout();
void onGatewayRxError();

void performReadings();
void updateTimeSNTP();
void onTimeUpdated(struct timeval *t );
void firmwareUpdateProgressCallback(HttpEvent_t *event);

void configSetup();
void gatewaySetup();

void configLoop();
void gatewayLoop();

void initialize() {
  if (!storageReadInitialized()) {
    resetStorageAndHardware();
    mp2667.reset();
    mp2667.enableShippingMode();
    storageWriteInitialized(true);
    led::redOn();
    while (true) {}
  } 
  if (!storageReadInitializedAfterUpdate()) {
    initializeAfterUpdate();
  }
  gShippingModeDisabled = storageReadShippingDisabled();
}

void initializeAfterUpdate() {
  // TODO: Do something here
  storageWriteInitializedAfterUpdate(true);
}

void batteryGuard() {
  battery::enable();
  uint16_t batteryVoltage = battery::readVoltage();
  uint8_t chargingStatus = mp2667.getChargingStatus();
  if (batteryVoltage < VBAT_MIN && chargingStatus == 0) {
#if defined(DEBUG) && DEBUG > 0
    Serial.printf("[SYSTEM]  Battery low: %d mV\r\n", batteryVoltage);
#endif
    rtc::clearAlarms();
    rtc::disableAlarm0();
    mp2667.enableShippingMode();
  } else {
    gBattery = battery::batteryVoltageToPercentage(batteryVoltage);
#if defined(DEBUG) && DEBUG > 0
    Serial.printf("[SYSTEM]  Battery good: %d mV\r\n", batteryVoltage);
#endif
  }
}

void resetStorageAndHardware() {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[SLEEPTEST]   RESET SOTRAGE AND HARDWARE"));
#endif
  clearPairedSensors();
  storageResetDefaults();
  rtc::clearAlarms();
  rtc::disableAlarm0();
  mp2667.reset();
}

void systemReset() {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[SLEEPTEST]   SYSTEM RESET"));
#endif
  resetStorageAndHardware();
  if (!sd::enable()) {
    return;
  }

  if (!sd::exists(FACTORY_BIN_PATH)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory program doesn't exist"));
#endif
    return;
  }

  if (sd::isDir(FACTORY_BIN_PATH)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory program is a directory"));
#endif
    return;
  }

  File factoryBin = SD.open(FACTORY_BIN_PATH.c_str());

  if (!factoryBin) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Failed to open factory program"));
#endif
    return;
  }

  uint32_t factoryBinSize = factoryBin.size();
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[RESET][ERROR]   Factory program size: "));
  Serial.println(factoryBinSize);
#endif

  if (factoryBinSize <= 0) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory program size is not positive"));
#endif
    return;
  }

  if (!Update.begin(factoryBinSize)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory update begin failed"));
#endif
    return;
  }

  uint32_t written = Update.writeStream(factoryBin);

  if (written != factoryBinSize) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Not entire factory file was loaded"));
#endif
    return;
  }

  if (!Update.end()) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory update end failed"));
#endif
    return;
  }

  if (!Update.isFinished()) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RESET][ERROR]   Factory update is not finished, but should be"));
#endif
    return;
  }

#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[RESET]          System reset done"));
  Serial.println(F("[RESET]          Restarting..."));
#endif
  delay(500);
  ESP.restart();
}

void updateFirmware(SafeString &firmwareUpdateVersion) {
  static HttpsOTAStatus_t otaStatus;
  delay(500); //wait for app to listen

  if (!gWifiSsid.isEmpty()) {
    if (!wifi::enabled) {
      wifi::enable(gWifiSsid, gWifiPassword);
    }
    if (!wifi::isConnected()) {
      wifi::connect();
    }

    uint32_t connectBeginMillis = millis();
    bool wifiConnectTimeout = false;
    while (!wifi::isConnected()) {
      delay(500);
      if (millis() - connectBeginMillis > WIFI_CONNECT_TIMEOUT_MS) {
        wifiConnectTimeout = true;
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[WIFI]    Failed to connect to wifi"));
#endif
        break;
      }
    }

    if (!wifiConnectTimeout) {
      createSafeString(firmwareUpdateUrl, 128);
      firmwareUpdateUrl = "https://";
      firmwareUpdateUrl += gStaticDomain;
      firmwareUpdateUrl += UPDATE_PATH_PREFIX;
      firmwareUpdateUrl += firmwareUpdateVersion;
      firmwareUpdateUrl += ".bin";

      HttpsOTA.onHttpEvent(firmwareUpdateProgressCallback);
#if defined(DEBUG) && DEBUG > 0
      Serial.printf("[SYSTEM]  Update url: %s\r\n", firmwareUpdateUrl.c_str());
      HttpsOTA.begin(firmwareUpdateUrl.c_str(), gCACert.c_str(), gHTTPUserAgent.c_str(), HTTP_TIMEOUT_MS, gDevAuthUser.c_str(), gDevAuthPassword.c_str(), false);
#else
      HttpsOTA.begin(firmwareUpdateUrl.c_str(), gCACert.c_str(), gHTTPUserAgent.c_str(), HTTP_TIMEOUT_MS , false);
#endif
      bool otaDone = false;
      while (true) {
        switch (HttpsOTA.status()) {
          case HTTPS_OTA_SUCCESS:
#if defined(DEBUG) && DEBUG > 0
            Serial.println("[OTASTATUS] SUCCESS");
#endif
            bleSetFirmwareUpdateStatus(1, 0);
            otaDone = true;
            break;
          case HTTPS_OTA_FAIL:
#if defined(DEBUG) && DEBUG > 0
            Serial.println("[OTASTATUS]  FAIL");
#endif
            bleSetFirmwareUpdateStatus(-1, 0);
            otaDone = true;
            break;
          case HTTPS_OTA_IDLE:
#if defined(DEBUG) && DEBUG > 0
            Serial.println(F("[UPDATE]  IDLE"));
            delay(500);
#endif
            break;
          case HTTPS_OTA_UPDATING:
#if defined(DEBUG) && DEBUG > 0
            Serial.println(F("[UPDATE]  UPDATING"));
            delay(500);
#endif
            break;
          case HTTPS_OTA_ERR:
#if defined(DEBUG) && DEBUG > 0
            Serial.println(F("[UPDATE]  ERROR"));
            delay(500);
#endif
            bleSetFirmwareUpdateStatus(-1, 0);
            otaDone = true;
            break;
            break;
        }
        delay(5);
        if (otaDone) { break; }
      }
    } else {
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[WIFI]    Connection timeout"));
#endif
      bleSetFirmwareUpdateStatus(-1, 0);
    }
  } else {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[WIFI]    No network supplied"));
#endif
    bleSetFirmwareUpdateStatus(-1, 0);
  }
  configState = BLE_CS;
}

void startPowerOffTimer() {
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[SYSTEM]  Automatic power off in %d seconds\r\n", CONFIG_IDLE_TIME_S);
#endif
  if (powerOffTimer == NULL) {
    powerOffTimer = timerBegin(0, 80, true);
    timerAttachInterrupt(powerOffTimer, &onPowerOffTimer, true);
    timerAlarmWrite(powerOffTimer, CONFIG_IDLE_TIME_S * S_TO_US, true);
  }
  timerRestart(powerOffTimer);
  timerAlarmEnable(powerOffTimer);
}

void stopPowerOffTimer() {
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SYSTEM]  Stop power off timer"));
#endif
  timerAlarmDisable(powerOffTimer);
}

void IRAM_ATTR onPowerOffTimer() {
  configState = SLEEP_CS;
}

void startSleep(bool buttonTriggered) {
  if (functionState  == CONFIG_S) {
    ble::stop();
  }

  vext::off();
  vext::disable();
  lora::off();
  Wire.end();
  pinMode(PIN_LORA_NSS, ANALOG);

  led::allOff();
  led::disable();

  //gpio_hold_en(GPIO_NUM_4);
  //gpio_deep_sleep_hold_en();
  // Prevente turning on Gateway after LED has turned off
  if (buttonTriggered) {
    delay(BUTTON_DURATION_TO_SLEEP_MS);
  }
  esp_sleep_enable_ext1_wakeup(0x10000, ESP_EXT1_WAKEUP_ALL_LOW);
  esp_sleep_enable_ext0_wakeup(GPIO_NUM_6, 0);
  esp_deep_sleep_start();
}

uint32_t intervalToRxTimeout(uint8_t interval) {
  uint32_t rxTimeout = lora::RX_TIMEOUT_PER_INTERVAL_MINUTE_MS * interval;
  if (rxTimeout < MIN_RX_TIMEOUT_MS) {
    rxTimeout = MIN_RX_TIMEOUT_MS;
  }
  return rxTimeout;
}

DateTime findNextWakeup(int32_t driftCompensationDiff) {
  uint8_t cyclesMissed = (rtc::getTime().unixtime() - timeWakeUp) / (gInterval * M_TO_S) + 1;
#if defined(DEBUG) && DEBUG > 0
          Serial.print("[SLEEPTEST] Cycles missed: ");
          Serial.println(cyclesMissed);
#endif
  uint16_t totalMinutes = wakeupInterval * cyclesMissed;
  uint8_t days = totalMinutes / (24 * 60);       // Calculate the number of days
  uint16_t remainingMinutesAfterDays = totalMinutes % (24 * 60); // Remaining minutes after calculating days
  uint8_t hours = remainingMinutesAfterDays / 60;            // Calculate the number of hours
  uint8_t minutes = remainingMinutesAfterDays % 60;
  if (driftCompensationDiff >= 0) {
#if defined(DEBUG) && DEBUG > 0
    Serial.print("[SLEEPTEST] Make driftCompensation longer: ");
    Serial.println(abs(driftCompensationDiff));
#endif
    return DateTime(timeWakeUp) + TimeSpan(days, hours, minutes, 0) - TimeSpan(abs(driftCompensationDiff * MS_TO_S));
  } else {
#if defined(DEBUG) && DEBUG > 0
    Serial.print("[SLEEPTEST] Make driftCompensation shorter: ");
    Serial.println(abs(driftCompensationDiff));
#endif
    return DateTime(timeWakeUp) + TimeSpan(days, hours, minutes, 0) + TimeSpan(abs(driftCompensationDiff * MS_TO_S));
  }
}

void setRTCAlarm(bool startNewCycle) {
  DateTime nextWakeup = DateTime(0);
  int32_t driftCompensationDiff = 0;
  
  switch (functionState) {
    case CONFIG_S: {
      if (startNewCycle) { // START NEW CYCLE
#if defined(DEBUG) && DEBUG > 0
          Serial.println("[SLEEPTEST] NEW CYCLE");
#endif
        rtc::clearAlarms();
        rtc::disableAlarm0();
        nextWakeup = DateTime(timeFirstPacketSent)
          + TimeSpan(0, 0, 0, PRECYCLE_DURATION_S)
          - TimeSpan(0, 0, 0, intervalToRxTimeout(gInterval) * MS_TO_S);
        rtc::setAlarm0(nextWakeup);
      } else { // IN A CYCLE
#if defined(DEBUG) && DEBUG > 0
          Serial.println("[SLEEPTEST] OLD CYCLE");
#endif
        if (gInterval != wakeupInterval) {
          // Interval was changed
#if defined(DEBUG) && DEBUG > 0
          Serial.print("[SLEEPTEST] gInterval: ");
          Serial.println(gInterval);
          Serial.print("[SLEEPTEST] wakeupInterval: ");
          Serial.println(wakeupInterval);
          Serial.print("[SLEEPTEST] gIntervalToRxTimeout: ");
          Serial.println(intervalToRxTimeout(gInterval));
          Serial.print("[SLEEPTEST] wakeupToRxTimeout: ");
          Serial.println(intervalToRxTimeout(wakeupInterval));
#endif
          driftCompensationDiff = intervalToRxTimeout(gInterval) - intervalToRxTimeout(wakeupInterval);
#if defined(DEBUG) && DEBUG > 0
          Serial.print("[SLEEPTEST] driftCompensationDiff: ");
          Serial.println(driftCompensationDiff);
#endif
        }
        if (timeWakeUp != 0 && rtc::getTime().unixtime() > timeWakeUp) { // MISSED WAKEUP
#if defined(DEBUG) && DEBUG > 0
          Serial.println("[SLEEPTEST] Missed wakeup");
#endif
          nextWakeup = findNextWakeup(driftCompensationDiff);
          rtc::setAlarm0(nextWakeup);
        } else {
#if defined(DEBUG) && DEBUG > 0
          Serial.println("[SLEEPTEST] Did not miss wakeup");
#endif
          if (driftCompensationDiff >= 0) {
#if defined(DEBUG) && DEBUG > 0
            Serial.print("[SLEEPTEST] Make driftCompensation longer: ");
            Serial.println(abs(driftCompensationDiff));
            Serial.print("[SLEEPTEST] timewakeup: ");
            printDate(DateTime(timeWakeUp));
            Serial.print("[SLEEPTEST] nextwakeup: ");
            printDate(nextWakeup);
#endif
            nextWakeup = DateTime(timeWakeUp) - TimeSpan(abs(driftCompensationDiff * MS_TO_S));
            rtc::setAlarm0(nextWakeup);
          } else {
#if defined(DEBUG) && DEBUG > 0
            Serial.print("[SLEEPTEST] Make driftCompensation shorter: ");
            Serial.println(abs(driftCompensationDiff));
            Serial.print("[SLEEPTEST] timewakeup: ");
            printDate(DateTime(timeWakeUp));
            Serial.print("[SLEEPTEST] nextwakeup: ");
            printDate(nextWakeup);
#endif
            nextWakeup = DateTime(timeWakeUp) + TimeSpan(abs(driftCompensationDiff * MS_TO_S));
            rtc::setAlarm0(nextWakeup);
          }
        }
      }
      break;
    }
    case GATEWAY_S: {
      if (timeWakeUp != 0 && rtc::getTime().unixtime() > timeWakeUp) { // MISSED WAKEUP
        nextWakeup = findNextWakeup(driftCompensationDiff);
        rtc::setAlarm0(nextWakeup);
      }
    }
  }
  if (nextWakeup.unixtime() != DateTime(0).unixtime()) {
    timeWakeUp = nextWakeup.unixtime();
  }

#if defined(DEBUG) && DEBUG > 0
  if (rtc::getAlarm0State()) {
    Serial.printf("[SLEEP]   Sleeping for %d minutes\r\n", (DateTime(timeWakeUp)-rtc::getTime()).minutes());
    Serial.printf("[SLEEP]   Sleeping for %d seconds\r\n", (DateTime(timeWakeUp)-rtc::getTime()).seconds());
  } else {
    Serial.println(F("[SLEEP]   Sleeping forever"));
  }
#endif
}

void pairBegin() {
  gIsPairing = true;
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[PAIR]    Pairing ready..."));
#endif
  configState = PAIR_CS;
  lora::receive();
}

void pairStartCycle() {
  TimeSpan timeTillSensorWakeup;
  
  uint8_t s = rtc::getTime().second();
  while (s == rtc::getTime().second()) {}
  if (!rtc::getAlarm0State()) {
    // Not in cycle
    if (gNumStartCyclePacketsSent == 0) {
      timeTillSensorWakeup = TimeSpan(0, 0, 0, PRECYCLE_DURATION_S);
    } else {
      uint8_t s = rtc::getTime().second();
      while (s == rtc::getTime().second()) {}
      timeTillSensorWakeup = TimeSpan(0, 0, 0, PRECYCLE_DURATION_S) - (rtc::getTime() - DateTime(timeFirstPacketSent));
    }
  } else {
    // Already in cycle
    uint8_t s = rtc::getTime().second();
    while (s == rtc::getTime().second()) {}
    if (rtc::getTime().unixtime() > timeWakeUp) { // MISSED WAKEUP
      uint8_t cyclesMissed = (rtc::getTime().unixtime() - timeWakeUp) / (gInterval * M_TO_S) + 1;
      uint16_t totalMinutes = gInterval * cyclesMissed;

      uint8_t days = totalMinutes / (24 * 60);       // Calculate the number of days
      uint16_t remainingMinutesAfterDays = totalMinutes % (24 * 60); // Remaining minutes after calculating days
      uint8_t hours = remainingMinutesAfterDays / 60;            // Calculate the number of hours
      uint8_t minutes = remainingMinutesAfterDays % 60;

      timeTillSensorWakeup = ((DateTime(timeWakeUp) + TimeSpan(days, hours, minutes, 0)) - rtc::getTime()) ;
    } else {
      timeTillSensorWakeup = (DateTime(timeWakeUp) - rtc::getTime());
    }   
  }
  uint8_t minutes = timeTillSensorWakeup.hours() * 60 + timeTillSensorWakeup.minutes();
  sendStartCycle(gInterval, minutes, timeTillSensorWakeup.seconds());
}

void onConfigTxDone() {
  timeLastPacketSent = rtc::getTime().unixtime();
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[LORA]    Config TX done"));
#endif
  switch (lastSentMessageCode) {
    case RESP_PAIR_CODE:
      lora::receive();
      break;
    case START_CYCLE_CODE:
      gNumStartCyclePacketsSent += 1;
      if (gNumStartCyclePacketsSent == 1) {
        timeFirstPacketSent = timeLastPacketSent;
        pairStartCycle();
      } else if (gNumStartCyclePacketsSent >= 2) {
        bool startNewCycle;
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[SLEEPTEST]    getAlarm0State: "));
  Serial.println(rtc::getAlarm0State());
#endif
        if (rtc::getAlarm0State()) {
          startNewCycle = false;
        } else {
          startNewCycle = true;
        }
        setRTCAlarm(startNewCycle);
        startSleep(false);
      } else {
        pairStartCycle();
      }
      break;
    default:
    break;
  }
}

void onConfigTxTimeout() {
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[LORA]    Config TX timeout"));
#endif
  //receiveOrTimeout(0);
}

void onConfigRxDone(uint8_t *payload, uint16_t size, int16_t rssi) {
  lora::state = LORA_RECEIVING_S;
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[LORA]    Received "));
#endif
  MessageCode code = (MessageCode)payload[0];
  switch (code) {
// ########################################
// ##########|   PAIR REQUEST   |##########
// ########################################
    case REQ_PAIR_CODE: {
      uint8_t sensorBytes[6];
      for (int i = 0; i < 6; i++) {
        sensorBytes[i] =  payload[i+1];
      }
      uint64_t sensor = u8BytesToU64(sensorBytes);
      int8_t sensorIndex = getPairedSensorIndex(sensor);
      if (sensorIndex < 0) { // New sensor
#if defined(DEBUG) && DEBUG > 0
        Serial.print(F("[PAIR]    New sensor: "));
        Serial.println(sensor);
#endif
        sensorIndex = newPairedSensor(sensor);
      } else {
#if defined(DEBUG) && DEBUG > 0
        Serial.print(F("[PAIR]    Old sensor: "));
        Serial.println(sensor);
#endif
      }

      sendPairResponse(sensorBytes, sensorIndex+1);
      createSafeString(bleSensorInfo, 24);
      copyBytesToAscii(bleSensorInfo, sensorBytes, 6);
      bleSensorInfo += " ";
      bleSensorInfo += (uint8_t) payload[7];
      bleSensorInfo += " ";
      bleSensorInfo += rssi;
      bleSensorInfo += " ";
      bleSensorInfo += sensorIndex;
      bleSetSensorInfo(bleSensorInfo);
      break;
    }
// #####################################
// ##########|   HEARTBEAT   |##########
// #####################################
    case HEARTBEAT_CODE: {
      uint8_t sensorBytes[6];
      for (int i = 0; i < 6; i++) {
        sensorBytes[i] =  payload[i+1];
      }
      uint64_t sensor = u8BytesToU64(sensorBytes);

      int8_t sensorIndex = getPairedSensorIndex(sensor);
      if (sensorIndex >= 0) {
        createSafeString(bleSensorInfo, 21);
        copyBytesToAscii(bleSensorInfo, sensorBytes, 6);
        bleSensorInfo += " ";
        bleSensorInfo += (uint8_t) payload[7];
        bleSensorInfo += " ";
        bleSensorInfo += rssi;
        bleSetSensorInfo(bleSensorInfo);
      }
      break;
    }
    default:
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[LORA]    Unknown packet"));
#endif
      break;
  }
}

void onGatewayTxDone() {
#if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[LORA]    Gateway TX done"));
    lora::off();
    gatewayState = PROCESS_READINGS_GS;
#endif
}
void onGatewayTxTimeout() {
}
void onGatewayRxDone(uint8_t *payload, uint16_t size, int16_t rssi) {
  lora::state = LORA_RECEIVING_S;
  MessageCode code = (MessageCode)payload[0];
  switch (code) {
    case REQ_READING_CODE: {
      uint8_t sensorBytes[6];
      for (int i = 0; i < 6; i++) {
        sensorBytes[i] =  payload[i+1];
      }
      uint64_t sensor = u8BytesToU64(sensorBytes);

      int8_t sensorIndex = getPairedSensorIndex(sensor);
      if (sensorIndex >= 0) {
        reading.sensorReadings[sensorIndex].mac = sensor;
        reading.sensorReadings[sensorIndex].bat = (payload[7] << 8) + payload[8];
        reading.sensorReadings[sensorIndex].temp = payload[9];
        reading.sensorReadings[sensorIndex].hum = payload[10];
        reading.sensorReadings[sensorIndex].accX = (payload[11] << 8) + payload[12];
        reading.sensorReadings[sensorIndex].accY = (payload[13] << 8) + payload[14];
        reading.sensorReadings[sensorIndex].accZ = (payload[15] << 8) + payload[16];
        for (int i=0; i<NUM_OF_FREQUENCY_BINS; i++) {
          reading.sensorReadings[sensorIndex].frequencyBins[i] = payload[17+i];
        }
        reading.sensorReadings[sensorIndex].soundLevel = payload[37];
        reading.sensorReadings[sensorIndex].rssi = (int8_t)rssi;
        setPairedSensorReadingReceived(sensor, true);
        numReadingsReceived += 1;
#if defined(DEBUG) && DEBUG > 0
        Serial.printf("[LORA]    Received reading from sensor %d\r\nTotal readings: %d\r\n", sensorIndex+1, numReadingsReceived);
#endif
      }
      if (numReadingsReceived == getNumPairedSensors()) {
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[LORA]    All readings received"));
#endif
        onGatewayRxTimeout();
      }
      break;
    }
    default:
      break;
  }
}
void onGatewayRxTimeout() {
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[LORA]    onGatewayRxTimeout"));
#endif
  uint8_t s = rtc::getTime().second();
  while (s == rtc::getTime().second()) {}
  TimeSpan timeTillSensorWakeup = (DateTime(timeWakeUp) + TimeSpan(0, 0, gInterval, 0))
                                - rtc::getTime() + TimeSpan(intervalToRxTimeout(gInterval) * MS_TO_S) ;
  uint8_t minutes = timeTillSensorWakeup.hours() * 60 + timeTillSensorWakeup.minutes();
  sendReadingResponse(gInterval, minutes, timeTillSensorWakeup.seconds());
}

void performReadings() {
  sht40.begin();
  sensors_event_t sht40_event;
  sht40.getTemperatureSensor()->getEvent(&sht40_event);
  reading.gatewayReading.temp = sht40_event.temperature;
  sht40.getHumiditySensor()->getEvent(&sht40_event);
  reading.gatewayReading.hum = sht40_event.relative_humidity;
  sht40.reset();

  mc3479.start(1);
  reading.gatewayReading.accX = mc3479.readRawAccel().XAxis;
  reading.gatewayReading.accY = mc3479.readRawAccel().YAxis;
  reading.gatewayReading.accZ = mc3479.readRawAccel().YAxis;
  mc3479.stop();

  reading.gatewayReading.bat = battery::readPercentage();

  reading.time = rtc::getTime();
}

void updateTimeSNTP() {
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[NTP]     Updating time"));
#endif
  sntp_set_time_sync_notification_cb(onTimeUpdated);
  configTime(0, 0, "pool.ntp.org", "time.nist.gov");
  uint32_t ntpUpdatebegin = millis();
  while (!timeAdjusted) {
    delay(500);
    if (millis() > ntpUpdatebegin + NTP_TIMEOUT_MS) {
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[NTP]     Failed to update time"));
#endif
      break;
    }
  }
}

void onTimeUpdated(struct timeval *t) {
  timeNTPUpdated = (uint64_t)t->tv_sec;
  DateTime ntpTime = DateTime(t->tv_sec);
  DateTime rtcTime = rtc::getTime();
  timeAdjusted = true;
  timeAdjustSeconds = ntpTime.unixtime() - rtcTime.unixtime();
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SYSTEM]  Time updated over SNTP"));
  Serial.print(F("[SYSTEM]  Adjusting with "));
  Serial.print(timeAdjustSeconds);
  Serial.println(F(" seconds"));
#endif
}

void firmwareUpdateProgressCallback(HttpEvent_t *event) {
  static uint32_t firmwareSize = 0;
  static uint32_t bytesDownloaded = 0;
    switch(event->event_id) {
      case HTTP_EVENT_ERROR:
#if defined(DEBUG) && DEBUG > 0
        Serial.println("[UPDATE]  Error");
#endif
        firmwareSize = 0;
        bytesDownloaded = 0;
        break;
      case HTTP_EVENT_ON_CONNECTED:
        break;
      case HTTP_EVENT_HEADER_SENT:
        break;
      case HTTP_EVENT_ON_HEADER:
        if (strcmp(event->header_key, "Content-Length") == 0) {
          firmwareSize = strtoul(event->header_value, NULL, 10);
        }
        break;
      case HTTP_EVENT_ON_DATA:
        bytesDownloaded += event->data_len;
        if (millis() - lastUpdateProgressMillis > BLE_UPDATE_PROGRESS_THROTTLE_MS || bytesDownloaded == firmwareSize) {
          uint8_t percent = (uint8_t) ((float)bytesDownloaded / (float)firmwareSize * 100);
          bleSetFirmwareUpdateStatus(0, percent);
#if defined(DEBUG) && DEBUG > 0
          Serial.printf("[UPDATE]:  Progress %d\%(%d/%d bytes)\r\n", percent, bytesDownloaded, firmwareSize);
#endif
          lastUpdateProgressMillis = millis();
        }
        break;
      case HTTP_EVENT_ON_FINISH:
#if defined(DEBUG) && DEBUG > 0
        Serial.println("[UPDATE]  Finished");
#endif
        firmwareSize = 0;
        bytesDownloaded = 0;
        break;
      case HTTP_EVENT_DISCONNECTED:
#if defined(DEBUG) && DEBUG > 0
        Serial.println("[UPDATE]  Disconnected");
#endif
        firmwareSize = 0;
        bytesDownloaded = 0;
        break;
    }
}

void configSetup() {
  functionState = CONFIG_S;
  configState = BOOTING_CS;
  storageReadToken(gToken);
  storageReadWifiSsid(gWifiSsid);
  storageReadWifiPassword(gWifiPassword);
  storageReadAPIDomain(gAPIDomain);
  storageReadStaticDomain(gStaticDomain);
  gInterval = storageReadInterval();
  // Interval at wakeup. Doesn't change when interval is updated over BLE
  wakeupInterval = gInterval;

  pinMode(GPIO_NUM_6, INPUT);
 #if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SYSTEM]  Config setup"));
#endif

  startPowerOffTimer();

  // Tests
  testSD(false);
  //while (bootProgress != 0b01) { // POWERTEST
  //  delay(500);                  // POWERTEST
  //  Serial.println(bootProgress);// POWERTEST
  //}
  testWifi(false);
}

void gatewaySetup() {
  storageReadToken(gToken);
  storageReadWifiSsid(gWifiSsid);
  storageReadWifiPassword(gWifiPassword);
  storageReadAPIDomain(gAPIDomain);
  storageReadStaticDomain(gStaticDomain);
  gInterval = storageReadInterval();
  // Interval at wakeup. Doesn't change when interval is updated over BLE
  wakeupInterval = gInterval;

  performReadings();
#if defined(DEBUG) && DEBUG > 0
  led::greenOn();
#endif
  lora::on();
  storageReadCACert(gCACert);
  functionState = GATEWAY_S;
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[SYSTEM]  Gateway setup"));
#endif

  if (timeAdjustSeconds != 0) {
    DateTime now = rtc::getTime();
    DateTime adjusted = now + TimeSpan(timeAdjustSeconds);
    rtc::setTime(adjusted);
    timeAdjustSeconds = 0;
  }

  if (!gWifiSsid.isEmpty()) {
    wifi::enable(gWifiSsid, gWifiPassword);
  }
  uint32_t timeout = SENSOR_SEND_INTERVAL * getNumPairedSensors() + intervalToRxTimeout(gInterval) * 2;
  lora::receive(timeout);
}

void configLoop() {
  switch (configState) {
  case BOOTING_CS:
    if (bootProgress == 0b11) {
      lora::on();
      createSafeString(gatewayBLEName, 21);
      gatewayBLEName = F("HIIVE-GW:");
      gatewayBLEName += gMacAscii;
      bleConfigSetup(gatewayBLEName);
      Serial.println("BOOT DONE");
      ble::start();
      led::allOff();
      led::cyan(16);
      configState = BLE_CS;
    }
    break;
  case BLE_CS:
    break;
  case PAIR_CS:
    switch(lora::state) {
      case LORA_STANDBY_S:
        break;
      case LORA_SENDING_S:
        if (millis() - lora::sendStartMillis > lora::TX_TIMEOUT_MS) {
          onConfigTxTimeout();
        }
        break;
      case LORA_SENT_S:
        onConfigTxDone();
        break;
      case LORA_RECEIVING_S:
        break;
      case LORA_RECEIVED_S:
        uint16_t length = lora::radio.getPacketLength();
        uint8_t data[length];
        lora::radio.readData(data, length);
        int16_t rssi = lora::radio.getRSSI();
        onConfigRxDone(data, length, rssi);
        break;
    }
    break;
  case UPDATE_CS:
      updateFirmware(gFirmwareUpdateVersion);
    break;
  case SLEEP_CS: 
#if defined(DEBUG) && DEBUG > 0
      Serial.print(F("[SLEEPTEST]    getAlarm0State: "));
      Serial.println(rtc::getAlarm0State());
#endif
      if (rtc::getAlarm0State()) {
        setRTCAlarm();
      }
      startSleep();
    break;
  default:
    break;
  }  
  if (button::pressed()) {
    if (millisButtonPressed == 0) {
      led::allOff();
      led::yellow(8);
      millisButtonStartPress = millis();
      millisButtonPressed = 1;
    } else {
      millisButtonPressed = millis() - millisButtonStartPress;
    }
  } else if ( millisButtonPressed != 0){
    millisButtonPressed = 0;
    led::allOff();
    if (gBleConnected) {
      led::blueOn();
    } else {
      led::cyan(16);
    }
    
  }
  if (millisButtonPressed > BUTTON_DURATION_TO_SLEEP_MS) {
#if defined(DEBUG) && DEBUG > 0
    Serial.print(F("[SLEEPTEST]    getAlarm0State: "));
    Serial.println(rtc::getAlarm0State());
#endif
    if (rtc::getAlarm0State()) {
      setRTCAlarm();
    }
    startSleep(true);
  }
}

void gatewayLoop() {
  switch (gatewayState) {
  case RECEIVE_GS:
    switch(lora::state) {
      case LORA_STANDBY_S:
        break;
      case LORA_SENDING_S:
        if (millis() - lora::sendStartMillis > lora::TX_TIMEOUT_MS) {
          onGatewayTxTimeout();
        }
        break;
      case LORA_SENT_S:
        onGatewayTxDone();
        break;
      case LORA_RECEIVING_S:
        if (lora::currentReceiveTimeout != 0 && millis() - lora::receiveStartMillis > lora::currentReceiveTimeout) {
          onGatewayRxTimeout();
        }
        break;
      case LORA_RECEIVED_S:
        uint16_t length = lora::radio.getPacketLength();
        uint8_t data[length];
        lora::radio.readData(data, length);
        int16_t rssi = lora::radio.getRSSI();
        onGatewayRxDone(data, length, rssi);
        break;
    }
    break;
  case PROCESS_READINGS_GS:
  {
    if (sd::enable()) {
      uint8_t status = storeReading(reading);
      reading.sdInfo.status = status;
      reading.sdInfo.spaceUsed = sd::spaceUsed();
    } else {
      reading.sdInfo.status = 1;
      reading.sdInfo.spaceUsed = 0;
    }
    sd::disable();

    if (!gWifiSsid.isEmpty()) {
      if (!wifi::enabled ) {
        wifi::enable(gWifiSsid, gWifiPassword);
      }
      if (!wifi::isConnected()) {
        wifi::connect();
      }
  
      uint32_t connectBeginMillis = millis();
      bool wifiConnectTimeout = false;
      while (!wifi::isConnected()) {
        delay(500);
        if (millis() - connectBeginMillis > WIFI_CONNECT_TIMEOUT_MS) {
#if defined(DEBUG) && DEBUG > 0
          Serial.println("[WIFI]    Failed to connect to wifi");
#endif
          wifiConnectTimeout = true;
          break;
        }
      }
  
      if (!wifiConnectTimeout) {
        createSafeString(readingJsonString, READING_JSON_SIZE);
        reading.wifiInfo.rssi = wifi::getRSSI();
        readingToJson(reading, readingJsonString);
        uint16_t sendReadingStatus = sendReadingHTTP(readingJsonString);
#if defined(DEBUG) && DEBUG > 0
        Serial.printf("[HTTP]    Send reading status: %d\r\n", sendReadingStatus);
        Serial.println(readingJsonString);
#endif
      }
    }
    gatewayState = NTP_GS;
    break;
  }
  case NTP_GS:
  {
    if (!gWifiSsid.isEmpty()) {
      DateTime now = rtc::getTime();
      DateTime lastUpdate = DateTime(timeNTPUpdated);
      TimeSpan timeDelta = now-lastUpdate;
      if (timeDelta.days() >= NTP_UPDATE_INTERVAL_DAYS) {
        updateTimeSNTP();
      }
    }
    gatewayState = SLEEP_GS;
    break;
  }
  case SLEEP_GS: 
    setRTCAlarm();
    startSleep();
    break;
  default:
    break;
  }  
};
void setup() {
  led::enable();
  led::allOff();

#if defined(DEBUG) && DEBUG > 0
  Serial.begin(115200);
  SafeString::setOutput(Serial);
#endif

  // EXT_1 wakeup pin
  pinMode(PIN_WAKEUP_EXT1, INPUT);

  wakeupCause = esp_sleep_get_wakeup_cause();
  if (wakeupCause == ESP_SLEEP_WAKEUP_EXT0) {
    uint32_t bootMillis = millis();
    led::yellow(8);
    while (millis() - bootMillis < BUTTON_DURATION_TO_SLEEP_MS) {
      if (!button::pressed()) {
        startSleep();
      }
    }
    led::allOff();
    led::orange(16);
  }

  uint64_t mac = ESP.getEfuseMac();
  U64MacToBytes(gMacBytes, mac);

  copyBytesToAscii(gMacAscii, gMacBytes, 6);

  gHTTPUserAgent = "hiive-gateway/";
  gHTTPUserAgent += FIRMWARE_VERSION;
  gHTTPUserAgent += " ";
  gHTTPUserAgent += gMacAscii;

#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[SYSTEM]  MAC: %s\r\n", gMacAscii.c_str());
#endif

  vext::enable();
  vext::on();
  Wire.begin(47, 38);
  Wire.setClock(100000);
  delay(500);

  Mcu.begin();

  rtc::init();
  bool isAlarm0 = rtc::getAlarm0();
  bool isAlarm1 = rtc::getAlarm1();
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[RTC]     Time now   : "));
  printDate(rtc::getTime());
  Serial.print(F("[RTC]     Time wakeup: "));
  printDate(DateTime(timeWakeUp));
  Serial.printf("[RTC]     Boot alarm0: %d\r\n", isAlarm0);
  Serial.printf("[RTC]     Boot alarm1: %d\r\n", isAlarm1);
#endif

  rtc::clearAlarms();

  batteryGuard();

  // Doesn't return on first run
  initialize();

  switch (wakeupCause) {
    case ESP_SLEEP_WAKEUP_EXT0:
 #if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[SLEEP]   Wakeup reason: Button"));
#endif
      configSetup();
      break;
    case ESP_SLEEP_WAKEUP_EXT1: {
      if (isAlarm0 || isAlarm1) {
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[SLEEP]   Wakeup reason: RTC"));
#endif
        gatewaySetup();
      } else {
#if defined(DEBUG) && DEBUG > 0
        Serial.println(F("[SLEEP]   Wakeup reason: IMU????"));
#endif
        startSleep();
      }
      break;
    }
    default:
#if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[SLEEP]   Wakeup reason: Other"));
#endif
    rtc::disableAlarm0();
    if (gShippingModeDisabled) {
      systemReset();
    } else {
      storageWriteShippingDisabled(true);
    }
      configSetup();
      break;
  }
}

void loop() {
  switch (functionState) {
  case CONFIG_S:
    configLoop();
    break;
  case GATEWAY_S:
    gatewayLoop();
    break;
  }
  delay(5);
}
