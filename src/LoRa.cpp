#include "LoRa.h"
#include "Defines.h"
#include "Hardware.h"

namespace lora {
  volatile LoRaState state;
  SX1262 radio = new Module(PIN_LORA_NSS, PIN_LORA_DIO_1, PIN_LORA_RESET, PIN_LORA_BUSY);

  uint32_t sendStartMillis;
  uint32_t receiveStartMillis;
  uint16_t currentReceiveTimeout;

  void IRAM_ATTR internalOnTxDone() {
    state = LORA_SENT_S;
  }
  void IRAM_ATTR internalOnRxDone() {
    state = LORA_RECEIVED_S;
  }
  
  int16_t on() {
    SPI.end();
    state = LORA_STANDBY_S;
    pinMode(PIN_SD_CS, OUTPUT);
    pinMode(PIN_LORA_CS, OUTPUT);
    digitalWrite(PIN_SD_CS, 1); // deselect
    digitalWrite(PIN_LORA_CS, 0); // select

    return radio.begin(LORA_FREQUENCY, LORA_BANDWITH, LORA_SPREADING_FACTOR, LORA_CODING_RATE,
                       LORA_SYNC_WORD, LORA_TRANSMISSION_POWER, LORA_PREAMBLE_LENGTH);  
  }
  int16_t off() {
    return radio.sleep();
  }
  int16_t standby() {
    return radio.standby();
  }
  int16_t send(uint8_t* data, uint8_t length) {
    radio.setPacketSentAction(internalOnTxDone);
    sendStartMillis = millis();
    state = LORA_SENDING_S;
    return radio.startTransmit(data, length);
  }
  int16_t receive(uint16_t timeout) {
    radio.setPacketReceivedAction(internalOnRxDone);
    currentReceiveTimeout = timeout;
    receiveStartMillis = millis();
    state = LORA_RECEIVING_S;
    return radio.startReceive();
  }
}
