#include "SDControl.h"

namespace sd {
  bool enable() {
    // Enable SD, disable LORA
    SPI.end();
    pinMode(PIN_SD_CS, OUTPUT);
    pinMode(PIN_LORA_CS, OUTPUT);
    digitalWrite(PIN_SD_CS, 0); // select
    digitalWrite(PIN_LORA_CS, 1); // deselect
    SPI.begin(PIN_SD_SCK, PIN_SD_MISO, PIN_SD_MOSI, PIN_SD_CS);
    if (!SD.begin(PIN_SD_CS, SPI)) {
      disable();
      return 0;
    }
    return 1;
  }
  
  void disable() {
    SD.end();
  }
  
  bool createFile(SafeString &filePath) {
    if (SD.exists(filePath.c_str())) {
      return true;
    }
    File file = SD.open(filePath.c_str(), FILE_WRITE);
    if (!file) { return false; }
  
    file.print(F("")); // TODO: check if it creates a file
    file.flush();
    file.close();
    if (!SD.exists(filePath.c_str())) { return false; }
  
    return true;
  }
  
  bool removeFile(SafeString &filePath) {
    return SD.remove(filePath.c_str());
  }
  
  bool createDir(SafeString &dirPath) {
    if (!SD.mkdir(dirPath.c_str())) { return false; }
    if (!SD.exists(dirPath.c_str())) { return false; }
    return true;
  }
  
  bool removeDir(SafeString &dirPath) {
    return SD.rmdir(dirPath.c_str());
  }
  
  bool writeFile(SafeString &filePath, SafeString &content) {
    if (!SD.exists(filePath.c_str())) { return false; }
    File file = SD.open(filePath.c_str(), FILE_WRITE);
    if (!file) { return false; }
    if(!file.write((uint8_t*) content.c_str(), content.length())) {
      return false;
    }
  
    file.close();
    return true;
  }
  
  bool readFile(SafeString &filePath, SafeString &buf) {
    File file = SD.open(filePath.c_str());
    if (!file) { return false; }
    uint64_t fileSize = file.size();
    if (fileSize > buf.capacity()) { return false; }
    uint64_t freeHeap = ESP.getFreeHeap();
    if (freeHeap < fileSize) { return false; }
    char* _buf = (char*)malloc(fileSize+1);
    if (!_buf) { return false; }
    uint64_t bytesRead = file.readBytes(_buf, fileSize);
    if (bytesRead != fileSize) { return false; }
    file.close();
    _buf[fileSize] = '\0';
  
    buf.readFrom(_buf);
    free(_buf);
    return true;
  }
  
  bool exists(SafeString &path) {
    return SD.exists(path.c_str());
  }
  
  bool isDir(SafeString &path) {
    File f = SD.open(path.c_str(), FILE_READ);
    bool isDir = f.isDirectory();
    f.close();
    return isDir;
  }
  
  int8_t spaceUsed() {
    uint64_t totalBytes = SD.totalBytes();
    if (totalBytes == 0) {
      return -1;
    }
    uint64_t usedBytes = SD.usedBytes();
    return (uint8_t)(((float)usedBytes / totalBytes) * 100);
  }
}
