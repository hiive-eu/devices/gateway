#include "Utils.h"

void copyBytesToAscii(SafeString &ascii, uint8_t *bytes, uint8_t size) {
  ascii.clear();
  for (int i = size-1; i >= 0; i--) {
    uint8_t byte = bytes[i];
    if (byte < 16) {
      ascii.print(0);
    }
    ascii.print(byte, HEX);
  }
}

void copySensorMacBytesToAscii(SafeString &ascii, uint8_t *bytes) {
  ascii.clear();
  for (int i = 0; i < 6; i++) {
    uint8_t byte = bytes[i];
    if (byte < 16) {
      ascii.print(0);
    }
    ascii.print(byte, HEX);
  }
}

void U64MacToBytes(uint8_t macBytes[6], uint64_t mac) {
  uint8_t bytes[6];
  memcpy(bytes, &mac, sizeof(bytes));
  macBytes[0] = bytes[5];
  macBytes[1] = bytes[4];
  macBytes[2] = bytes[3];
  macBytes[3] = bytes[2];
  macBytes[4] = bytes[1];
  macBytes[5] = bytes[0];
}

uint64_t u8BytesToU64(uint8_t u8[6]) {
  uint64_t u64 = 0;
  memcpy(&u64, u8, 6);
  return u64;
}

void printDate(DateTime t) {
  char buf[32];
  sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d",
            t.year(), t.month(), t.day(), t.hour(), t.minute(),
            t.second());
  Serial.println(F(buf));
}
void printDate(uint64_t t) {
  DateTime d = DateTime(t);
  char buf[32];
  sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d",
            d.year(), d.month(), d.day(), d.hour(), d.minute(),
            d.second());
  Serial.println(F(buf));
}
