#include "Hardware.h"

namespace vext {
  void enable() {
    pinMode(PIN_DOUT_VEXT, OUTPUT);
  }
  
  void disable() {
    pinMode(PIN_DOUT_VEXT, INPUT);
  }
  
  void on() {
    digitalWrite(PIN_DOUT_VEXT, 0);
    delay(200);
  }
  
  void off() {
    digitalWrite(PIN_DOUT_VEXT, 1);
    delay(200);
  }
}

namespace button {
  bool pressed() {
    return !digitalRead(PIN_DIN_BUTTON);
  }
}
