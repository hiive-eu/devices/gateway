#include "Reading.h"
#include "BLEConfig.h"
#include "FS.h"
#include "SafeString.h"
#include "Sensors.h"
#include "TaskFunctions.h"
#include "Utils.h"
#include <cstring>

createSafeString(DIRECTORY_PATH, DIRECTORY_PATH_SIZE, "/readings");
createSafeString(filePath, FILE_PATH_SIZE);

void readingToJson(Reading reading, SafeString &readingJsonString) {
  readingJsonString.clear();
  JsonDocument jsonReading;

  char date[21];
  snprintf(date, 20, "%04d-%02d-%02dT%02d:%02d:%02d", reading.time.year(),
           reading.time.month(), reading.time.day(), reading.time.hour(), reading.time.minute(),
           reading.time.second());
  date[19] = 'Z';
  date[20] = '\0';
  jsonReading["t"] = date;

  //JsonObject sd = jsonReading.createNestedObject("sd");
  JsonObject sd = jsonReading["sd"].to<JsonObject>();
  sd["s"] = reading.sdInfo.status;
  sd["u"] = reading.sdInfo.spaceUsed;

  //JsonObject wl = jsonReading.createNestedObject("wl");
  JsonObject wl = jsonReading["wl"].to<JsonObject>();
  wl["r"] = reading.wifiInfo.rssi;

  //JsonObject g = jsonReading.createNestedObject("g");
  JsonObject g = jsonReading["g"].to<JsonObject>();
  g["m"] = gMacAscii.c_str();
  g["b"] = reading.gatewayReading.bat;
  g["t"] = reading.gatewayReading.temp;
  g["h"] = reading.gatewayReading.hum;
  g["x"] = reading.gatewayReading.accX;
  g["y"] = reading.gatewayReading.accY;
  g["z"] = reading.gatewayReading.accZ;

  //JsonArray s = jsonReading.createNestedArray("s");
  JsonArray s = jsonReading["s"].to<JsonArray>();

  uint8_t numReadings = 0;
  for (int i=0; i < MAX_SENSORS; i++) {
    if (reading.sensorReadings[i].mac != 0) {
      numReadings += 1;
    }
  }

  JsonObject readings[numReadings];

  int nextSensorIndex = 0;
  for (int i=0; i < MAX_SENSORS; i++) {
    if (reading.sensorReadings[i].mac != 0) {
      createSafeString(sensorMac, 12);
      uint8_t bytes[6];
      U64MacToBytes(bytes, reading.sensorReadings[i].mac);
      copySensorMacBytesToAscii(sensorMac, bytes);
      readings[nextSensorIndex] = s.add<JsonObject>();
      readings[nextSensorIndex]["m"] = sensorMac;
      readings[nextSensorIndex]["b"] = reading.sensorReadings[i].bat;
      readings[nextSensorIndex]["t"] = reading.sensorReadings[i].temp;
      readings[nextSensorIndex]["h"] = reading.sensorReadings[i].hum;
      readings[nextSensorIndex]["x"] = reading.sensorReadings[i].accX;
      readings[nextSensorIndex]["y"] = reading.sensorReadings[i].accY;
      readings[nextSensorIndex]["z"] = reading.sensorReadings[i].accZ;
      JsonArray sf = readings[nextSensorIndex]["f"].to<JsonArray>();
      for (int j=0; j<NUM_OF_FREQUENCY_BINS; j++) {
        sf[j] = reading.sensorReadings[i].frequencyBins[j];
      }
      readings[nextSensorIndex]["l"] = reading.sensorReadings[i].soundLevel;
      readings[nextSensorIndex]["r"] = reading.sensorReadings[i].rssi;
      nextSensorIndex += 1;
    }
  }

  char tmp[READING_JSON_SIZE];

  jsonReading.shrinkToFit();  // optional
  serializeJson(jsonReading, tmp);

  readingJsonString = tmp;
}

void readingToByteArray(Reading reading, uint8_t numReadings, uint8_t* outArray) {
  uint8_t *outArrayIndex = outArray;

  // Number of sensor readings
  outArray[0] = numReadings;
  outArrayIndex += 1;

  // Gateway reading
  memcpy(outArrayIndex, &reading.gatewayReading.bat, sizeof(reading.gatewayReading.bat));
  outArrayIndex += sizeof(reading.gatewayReading.bat);
  memcpy(outArrayIndex, &reading.gatewayReading.temp, sizeof(reading.gatewayReading.temp));
  outArrayIndex += sizeof(reading.gatewayReading.temp);
  memcpy(outArrayIndex, &reading.gatewayReading.hum, sizeof(reading.gatewayReading.hum));
  outArrayIndex += sizeof(reading.gatewayReading.hum);
  memcpy(outArrayIndex, &reading.gatewayReading.accX, sizeof(reading.gatewayReading.accX));
  outArrayIndex += sizeof(reading.gatewayReading.accX);
  memcpy(outArrayIndex, &reading.gatewayReading.accY, sizeof(reading.gatewayReading.accY));
  outArrayIndex += sizeof(reading.gatewayReading.accY);
  memcpy(outArrayIndex, &reading.gatewayReading.accZ, sizeof(reading.gatewayReading.accZ));

  // Sensor readings
  for (int i=0; i<MAX_SENSORS; i++) {
    if (reading.sensorReadings[i].mac != 0) {
      memcpy(outArrayIndex, &reading.sensorReadings[i].mac, sizeof(reading.sensorReadings[i].mac));
      outArrayIndex += sizeof(reading.sensorReadings[i].mac);
      memcpy(outArrayIndex, &reading.sensorReadings[i].bat, sizeof(reading.sensorReadings[i].bat));
      outArrayIndex += sizeof(reading.sensorReadings[i].bat);
      memcpy(outArrayIndex, &reading.sensorReadings[i].temp, sizeof(reading.sensorReadings[i].temp));
      outArrayIndex += sizeof(reading.sensorReadings[i].temp);
      memcpy(outArrayIndex, &reading.sensorReadings[i].hum, sizeof(reading.sensorReadings[i].hum));
      outArrayIndex += sizeof(reading.sensorReadings[i].hum);
      memcpy(outArrayIndex, &reading.sensorReadings[i].accX, sizeof(reading.sensorReadings[i].accX));
      outArrayIndex += sizeof(reading.sensorReadings[i].accX);
      memcpy(outArrayIndex, &reading.sensorReadings[i].accY, sizeof(reading.sensorReadings[i].accY));
      outArrayIndex += sizeof(reading.sensorReadings[i].accY);
      memcpy(outArrayIndex, &reading.sensorReadings[i].accZ, sizeof(reading.sensorReadings[i].accZ));
      outArrayIndex += sizeof(reading.sensorReadings[i].accZ);
      memcpy(outArrayIndex, reading.sensorReadings[i].frequencyBins, sizeof(reading.sensorReadings[i].frequencyBins));
      outArrayIndex += sizeof(reading.sensorReadings[i].frequencyBins);
      memcpy(outArrayIndex, &reading.sensorReadings[i].soundLevel, sizeof(reading.sensorReadings[i].soundLevel));
      outArrayIndex += sizeof(reading.sensorReadings[i].soundLevel);
      memcpy(outArrayIndex, &reading.sensorReadings[i].rssi, sizeof(reading.sensorReadings[i].rssi));  
    }
  }
}

uint8_t storeReading(Reading reading) {
  // Check if node with directory name exists
  if (!SD.exists(DIRECTORY_PATH.c_str())) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Creating readings directory");
#endif
    if (!sd::createDir(DIRECTORY_PATH)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Failed to create directory");
#endif
      return 2;
    }
  }

  // Check if node with directory name is a directory.
  // If not delete file and create directory
  File dir = SD.open(DIRECTORY_PATH.c_str());
  if (!dir) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Failed to open readings file/dir");
#endif
    return 3;
  }
  if (!dir.isDirectory()) {
    dir.close();
    if (!SD.remove(DIRECTORY_PATH.c_str())) {
#if defined(DEBUG) && DEBUG > 0
      Serial.println("[SD] Failed to remove file with dir path name");
#endif
      return 2;
    }
    if (!sd::createDir(DIRECTORY_PATH)) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Failed to create directory");
#endif
      return 2;
    }
  }

  uint16_t year = reading.time.year();
  uint8_t month = reading.time.month();

  filePath.print(DIRECTORY_PATH);
  filePath.print(F("/"));
  filePath.print(year);
  filePath.print(F("-"));
  if (month < 10) {
    filePath.print(F("0"));
  }
  filePath.print(month);

  if (!SD.exists(filePath.c_str())) {
#if defined(DEBUG) && DEBUG > 0
      Serial.print("[SD] Creating file: ");
      Serial.println(filePath);
#endif
    if (!sd::createFile(filePath)) {
#if defined(DEBUG) && DEBUG > 0
      Serial.println("[SD] Failed to create readings file");
#endif
      return 2;
    }
  }

  File file = SD.open(filePath.c_str(), FILE_APPEND);

  if (!file) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Failed to open file for writing");
#endif
    return 2;
  }

  uint8_t numReadings = 0;
  for (int i=0; i < MAX_SENSORS; i++) {
    if (reading.sensorReadings[i].mac != 0) {
      numReadings += 1;
    }
  }

  uint32_t readingBytesSize = sizeof(numReadings) + sizeof(GatewayReading) + (numReadings * sizeof(SensorReading));

  uint8_t readingBytes[readingBytesSize];
  readingToByteArray(reading, numReadings, readingBytes);
  int charsWritten = file.write(readingBytes, readingBytesSize);
  if (!charsWritten) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[SD] Failed to write reading to file");
#endif
    return 2;
  }
  file.flush();
  file.close();
  return 0;
}

int8_t listReadingFiles(SafeString &outReadingFiles) {
  if (sd::enable()) {
    int8_t numFiles = 0;
    File readingsDir = SD.open(DIRECTORY_PATH.c_str());
    if (!readingsDir) {
#if defined(DEBUG) && DEBUG > 0
        Serial.println("[SD] Failed to open directory for file listing");
#endif
      return 0;
    }
    while (true) {
      File entry = readingsDir.openNextFile();
      if (!entry) {
        // No more files
        break;
      }
      if (!entry.isDirectory()) {
        numFiles += 1;
        outReadingFiles += entry.name();
      }
      entry.close();
    }
    return numFiles;
  } else {
    return -1;
  }
}

File getReadingFileHandle(char filename[8]) {
  if (sd::enable()) {
    createSafeString(filePath, FILE_PATH_SIZE);
    filePath = DIRECTORY_PATH;
    filePath += "/";
    filePath += filename;

    File file = SD.open(filePath.c_str());
    if (!file) {
      return File();
    } else {
      return file;
    }
  } else {
    return File();
  }
}

bool deleteReadingFile(char filename[8]) {
  if (sd::enable()) {
    createSafeString(filePath, FILE_PATH_SIZE);
    filePath = DIRECTORY_PATH;
    filePath += "/";
    filePath += filename;

    if (!SD.remove(filePath.c_str())) {
      return -1;
    } else {
      return 1;
    }
  } else {
    return 1;
  }
}

int16_t sendReadingHTTP(SafeString &readingJsonString) {
  HTTPClient readingHTTPClient;
#if defined(DEBUG) && DEBUG > 0
  readingHTTPClient.setAuthorization(gDevAuthUser.c_str(), gDevAuthPassword.c_str());
#endif
  readingHTTPClient.setUserAgent(gHTTPUserAgent.c_str());
  readingHTTPClient.setTimeout(HTTP_TIMEOUT_MS);
  readingHTTPClient.begin(gAPIDomain.c_str(), 443, READING_PATH.c_str(), gCACert.c_str());
  readingHTTPClient.addHeader("Content-Type", "application/json");
  readingHTTPClient.addHeader("Token", gToken.c_str());
  int16_t status = readingHTTPClient.POST(readingJsonString.c_str());
  readingHTTPClient.end();
  return status;
}

