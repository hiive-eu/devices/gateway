#include "LED.h"

namespace led {
  void enable() {
    ledcSetup(CHANNEL_R, RGB_FREQ, RGB_RESOLUTION);
    ledcAttachPin(PIN_PWM_RGB_R, CHANNEL_R);
  
    ledcSetup(CHANNEL_G, RGB_FREQ, RGB_RESOLUTION);
    ledcAttachPin(PIN_PWM_RGB_G, CHANNEL_G);
  
    ledcSetup(CHANNEL_B, RGB_FREQ, RGB_RESOLUTION);
    ledcAttachPin(PIN_PWM_RGB_B, CHANNEL_B);
  }
  
  void disable() {
    allOff();
    ledcDetachPin(PIN_PWM_RGB_R);
    ledcDetachPin(PIN_PWM_RGB_G);
    ledcDetachPin(PIN_PWM_RGB_B);
  }
  
  void redOn(uint8_t brightness) {
    ledcWrite(CHANNEL_R, brightness);
  }
  
  void redOff() {
    ledcWrite(CHANNEL_R, 0);
  }
  
  void greenOn(uint8_t brightness) {
    ledcWrite(CHANNEL_G, brightness);
  }
  
  void greenOff() {
    ledcWrite(CHANNEL_G, 0);
  }
  
  void blueOn(uint8_t brightness) {
    ledcWrite(CHANNEL_B, brightness);
  }
  
  void blueOff() {
    ledcWrite(CHANNEL_B, 0);
  }
  
  void allOff() {
    redOff();
    greenOff();
    blueOff();
  }
  
  void yellow(uint8_t brightness) {
    ledcWrite(CHANNEL_R, brightness);
    ledcWrite(CHANNEL_G, brightness);
  }
  
  void orange(uint8_t brightness) {
    ledcWrite(CHANNEL_R, brightness*2);
    ledcWrite(CHANNEL_G, brightness);
  }
  void cyan(uint8_t brightness) {
    ledcWrite(CHANNEL_G, brightness);
    ledcWrite(CHANNEL_B, brightness);
  }
}
