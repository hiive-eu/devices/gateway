#include "Protocol.h"
#include "LoRa.h"

void sendPairResponse(uint8_t sensorBytes[6], uint8_t sensorNumber) {
  delay(500); // Sending before sensors start listening
  uint8_t txBuffer[RESP_PAIR_PAYLOAD_SIZE];
  txBuffer[0] = RESP_PAIR_CODE;
  txBuffer[1] = sensorBytes[0];
  txBuffer[2] = sensorBytes[1];
  txBuffer[3] = sensorBytes[2];
  txBuffer[4] = sensorBytes[3];
  txBuffer[5] = sensorBytes[4];
  txBuffer[6] = sensorBytes[5];
  txBuffer[7] = sensorNumber;
  txBuffer[8] = gMacBytes[0];
  txBuffer[9] = gMacBytes[1];
  txBuffer[10] = gMacBytes[2];
  txBuffer[11] = gMacBytes[3];
  txBuffer[12] = gMacBytes[4];
  txBuffer[13] = gMacBytes[5];

#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[LORA]    Sending RESP_PAIR"));
#endif
  setLastSentMessage(RESP_PAIR_CODE);
  lora::send(txBuffer, RESP_PAIR_PAYLOAD_SIZE);
}

void sendReadingResponse(uint8_t interval, uint8_t minutes, uint8_t seconds) {
  uint8_t txBuffer[RESP_READING_PAYLOAD_SIZE];
  txBuffer[0] = RESP_READING_CODE;
  txBuffer[1] = gMacBytes[0];
  txBuffer[2] = gMacBytes[1];
  txBuffer[3] = gMacBytes[2];
  txBuffer[4] = gMacBytes[3];
  txBuffer[5] = gMacBytes[4];
  txBuffer[6] = gMacBytes[5];
  txBuffer[7] = interval;
  txBuffer[8] = minutes;
  txBuffer[9] = seconds;
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[LORA]    Sending RESP_READING"));
  Serial.print("[LORA] Sending interval: ");
  Serial.print(interval);
  Serial.print(" ");
  Serial.println(txBuffer[7]);
  Serial.print("[LORA] Sending minutes: ");
  Serial.print(minutes);
  Serial.print(" ");
  Serial.println(txBuffer[8]);
  Serial.print("[LORA] Sending seconds: ");
  Serial.print(seconds);
  Serial.print(" ");
  Serial.println(txBuffer[9]);
#endif
  setLastSentMessage(RESP_READING_CODE);
  lora::send(txBuffer, RESP_READING_PAYLOAD_SIZE);
}

void sendStartCycle(uint8_t interval, uint8_t minutesTillWakeup, uint8_t secondsTillWakeup) {
  uint8_t txBuffer[START_CYCLE_PAYLOAD_SIZE];
  txBuffer[0] = START_CYCLE_CODE;
  txBuffer[1] = gMacBytes[0];
  txBuffer[2] = gMacBytes[1];
  txBuffer[3] = gMacBytes[2];
  txBuffer[4] = gMacBytes[3];
  txBuffer[5] = gMacBytes[4];
  txBuffer[6] = gMacBytes[5];
  txBuffer[7] = interval;
  txBuffer[8] = minutesTillWakeup;
  txBuffer[9] = secondsTillWakeup;

#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[LORA]    Sending START_CYCLE"));
  Serial.printf("[LORA]    interval: %d\r\n", interval);
  Serial.printf("[LORA]    minutesTillWakeup: %d\r\n", minutesTillWakeup);
  Serial.printf("[LORA]    secondsTillWakeup: %d\r\n", secondsTillWakeup);
#endif
  setLastSentMessage(START_CYCLE_CODE);
  lora::send(txBuffer, START_CYCLE_PAYLOAD_SIZE);
}
