#include "BLEConfig.h"
#include "BLECharacteristic.h"
#include "LED.h"
#include "SafeString.h"
#include "TaskFunctions.h"

class SystemTimeCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    uint8_t *timeBytes = pCharacteristic->getData();
    uint64_t time = u8BytesToU64(timeBytes);
    DateTime dt = DateTime(time);
    rtc::setTime(dt);
    timeNTPUpdated = dt.unixtime();
    bleSetTime(time);
  }
};
class SystemIntervalCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    uint8_t interval = pCharacteristic->getData()[0];
    storageWriteInterval(interval);
    gInterval = interval;
  }
};
class SystemTokenCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gToken = pCharacteristic->getValue().c_str();
    storageWriteToken(gToken);
  }
};
class SystemResetCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    uint8_t val = pCharacteristic->getData()[0];
    systemReset();
  }
};
class SystemFirmwareUpdateCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gFirmwareUpdateVersion = pCharacteristic->getValue().c_str();
    configState = UPDATE_CS;
  }
};
class PairBeginCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    pairBegin();
  }
};
class PairStartCycleCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    pairStartCycle();
  }
};
class WifiSsidCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gWifiSsid = pCharacteristic->getValue().c_str();
    storageWriteWifiSsid(gWifiSsid);
  }
};
class WifiPasswordCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gWifiPassword = pCharacteristic->getValue().c_str();
    storageWriteWifiPassword(gWifiPassword);
  }
};
class WifiScanStartCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    BLECharacteristic *pWifiScanResultsChar = ble::pServer->getServiceByUUID(WIFI_SERVICE_UUID)->getCharacteristic(WIFI_SCAN_RESULTS_CHAR_UUID);
    scanWifi(pWifiScanResultsChar);
  }
};
class ServerAPIDomainCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gAPIDomain = pCharacteristic->getValue().c_str();
    storageWriteAPIDomain(gAPIDomain);
  }
};
class ServerStaticDomainCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    gStaticDomain = pCharacteristic->getValue().c_str();
    storageWriteStaticDomain(gStaticDomain);
  }
};
class TestCommandCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t * param) {
    uint8_t val = pCharacteristic->getData()[0];
    switch (val) {
    case 1:
      testWifi(true);
      break;
    case 2:
      testSD(true);
      break;
    }
  }
};
class ReadingsCommandCbk: public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic *pCharacteristic, esp_ble_gatts_cb_param_t *param) {
    uint8_t* bytes = pCharacteristic->getData();
    uint8_t command = bytes[0];
    BLECharacteristic *pReadingsDataChar = ble::pServer->getServiceByUUID(READINGS_SERVICE_UUID)->getCharacteristic(READINGS_DATA_CHAR_UUID);
    switch (command) {
      // Get filenames
      case 1:
        sendFilenames(pReadingsDataChar);
        break;
      // Get reading file
      case 2:
      {
        createSafeString(filename, 7);
        for (int i=1; i<8; i++) {
          filename += (char)bytes[i];
        }
        sendReading(filename, pReadingsDataChar);
        break;
      }
      // Delete reading file
      case 3:
      {
        createSafeString(filename, 7);
        for (int i=1; i<8; i++) {
          filename += (char)bytes[i];
        }
        deleteReadingFile(filename, pReadingsDataChar);
        break;
      }
      // Reading file transfer bytes received
      case 4:
      {
        gBytesReceivedReadingTransfer = ((int32_t)bytes[1] << 24) |
                                        ((int32_t)bytes[2] << 16) |
                                        ((int32_t)bytes[3] << 8)  |
                                        ((int32_t)bytes[4]);
        break;
      }
    }
  }
};

void bleSetTime(uint32_t time) {
  BLECharacteristic *timeChar = ble::pServer
    ->getServiceByUUID(SYSTEM_SERVICE_UUID)
    ->getCharacteristic(SYSTEM_TIME_CHAR_UUID);

  timeChar->setValue((uint8_t*)&time, 4);
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Time -> %d\r\n", time);
#endif
}
void bleSetBattery(uint8_t battery) {
  BLECharacteristic *batteryChar = ble::pServer
    ->getServiceByUUID(SYSTEM_SERVICE_UUID)
    ->getCharacteristic(SYSTEM_BATTERY_CHAR_UUID);

  batteryChar->setValue(&battery, 1);
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Battery -> %d\r\n", battery);
#endif
}
void bleSetInterval(uint8_t interval) {
  BLECharacteristic *intervalChar = ble::pServer
    ->getServiceByUUID(SYSTEM_SERVICE_UUID)
    ->getCharacteristic(SYSTEM_INTERVAL_CHAR_UUID);

  intervalChar->setValue(&interval, 1);
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Interval -> %d\r\n", interval);
#endif
}
void bleSetFirmwareVersion(SafeString &firmwareVersion) {
  BLECharacteristic *firmwareVersionChar = ble::pServer
    ->getServiceByUUID(SYSTEM_SERVICE_UUID)
    ->getCharacteristic(SYSTEM_FIRMWARE_VERSION_CHAR_UUID);

  firmwareVersionChar->setValue(firmwareVersion.c_str());
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Firmware version -> %s\r\n", firmwareVersion.c_str());
#endif
}
void bleSetFirmwareUpdateStatus(int8_t status, uint8_t progress) {
  BLECharacteristic *systemFirmwareUpdateChar = ble::pServer
    ->getServiceByUUID(SYSTEM_SERVICE_UUID)
    ->getCharacteristic(SYSTEM_FIRMWARE_UPDATE_CHAR_UUID);

  if (status == 1) {
      systemFirmwareUpdateChar->setValue((uint8_t*)&status, 1);
      systemFirmwareUpdateChar->notify();
  } else if (status == -1){
      systemFirmwareUpdateChar->setValue((uint8_t*)&status, 1);
      systemFirmwareUpdateChar->notify();
  } else {
  createSafeString(statusString, 5);
    statusString  = status;
    statusString += " ";
    statusString += progress;
    systemFirmwareUpdateChar->setValue(statusString.c_str());
    systemFirmwareUpdateChar->notify();
#if defined(DEBUG) && DEBUG > 0
    Serial.printf("[BLE]     Firmware update status -> %s\r\n", statusString.c_str());
#endif
  }
}
void bleSetSensorInfo(SafeString &sensorInfo) {
  BLECharacteristic *sensorInfoChar = ble::pServer
    ->getServiceByUUID(PAIR_SERVICE_UUID)
    ->getCharacteristic(PAIR_SENSOR_INFO_CHAR_UUID);

  sensorInfoChar->setValue(sensorInfo.c_str());
  sensorInfoChar->notify();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Sensor info -> %s\r\n", sensorInfo.c_str());
#endif
}
void bleSetWifiSsid(SafeString &ssid) {
  BLECharacteristic *ssidChar = ble::pServer
    ->getServiceByUUID(WIFI_SERVICE_UUID)
    ->getCharacteristic(WIFI_SSID_CHAR_UUID);

  ssidChar->setValue(ssid.c_str());
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Wifi ssid -> %s\r\n", ssid.c_str());
#endif
}
void bleSetWifiScanResults(SafeString &scanResult) {
  BLECharacteristic *wifiScanResultsChar = ble::pServer
    ->getServiceByUUID(WIFI_SERVICE_UUID)
    ->getCharacteristic(WIFI_SCAN_RESULTS_CHAR_UUID);

  wifiScanResultsChar->setValue(scanResult.c_str());
  wifiScanResultsChar->notify();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Wifi scan result -> %s\r\n", scanResult.c_str());
#endif
}
void bleSetAPIDomain(SafeString &domain) {
  BLECharacteristic *domainChar = ble::pServer
    ->getServiceByUUID(SERVER_SERVICE_UUID)
    ->getCharacteristic(SERVER_API_DOMAIN_CHAR_UUID);

  domainChar->setValue(domain.c_str());
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     API domain -> %s\r\n", domain.c_str());
#endif
}
void bleSetStaticDomain(SafeString &domain) {
  BLECharacteristic *domainChar = ble::pServer
    ->getServiceByUUID(SERVER_SERVICE_UUID)
    ->getCharacteristic(SERVER_STATIC_DOMAIN_CHAR_UUID);

  domainChar->setValue(domain.c_str());
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Static domain -> %s\r\n", domain.c_str());
#endif
}
void bleSetTestResult(TestResult &res) {
  BLECharacteristic *testWifiResultChar = ble::pServer
    ->getServiceByUUID(TEST_SERVICE_UUID)
    ->getCharacteristic(TEST_RESULT_CHAR_UUID);

  createSafeString(resString, 19);
  resString = res.wifi.res; // -1 = 4
  resString += "#";
  resString += res.wifi.rssi; // -100 = 4
  resString += " ";
  resString += res.sd.res; // -1 = 4
  resString += "#";
  resString += res.sd.spaceUsed; // 100 = 4

  testWifiResultChar->setValue(resString.c_str());
  testWifiResultChar->notify();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[BLE]     Test result -> %s\r\n", resString.c_str());
#endif
}

void onBLEConnect(BLEServer *pServer) {
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[BLE]     Connected: "));
#endif
  gBleConnected = true;
  if (!gIsPairing) {
    led::allOff();
    led::blueOn();
    stopPowerOffTimer();
  }
}

void onBLEDisconnect(BLEServer *pServer) {
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[BLE]     Disconnected"));
#endif
  gBleConnected = false;
  pServer->getAdvertising()->start();
  if (!gIsPairing) {
    led::allOff();
    led::cyan();
    startPowerOffTimer();
  }
}

void onBLEMtuChanged(BLEServer *pServer, esp_ble_gatts_cb_param_t *param) {
  gMtu = param->mtu.mtu;
#if defined(DEBUG) && DEBUG > 0
  Serial.print(F("[BLE]     MTU changed: "));
  Serial.println(param->mtu.mtu);
#endif
} 

void bleConfigSetup(SafeString &gatewayBLEName) {
  ble::init(gatewayBLEName, onBLEConnect, onBLEDisconnect, onBLEMtuChanged);

//
// SYSTEM
//
  BLEService *pSystemService = ble::createService(SYSTEM_SERVICE_UUID, 16);

// TIME
  pSystemService->createCharacteristic(
                    SYSTEM_TIME_CHAR_UUID,
                    BLECharacteristic::PROPERTY_READ |
                    BLECharacteristic::PROPERTY_WRITE
                  );
  pSystemService->getCharacteristic(SYSTEM_TIME_CHAR_UUID)->setCallbacks(new SystemTimeCbk);

// BATTERY
  pSystemService->createCharacteristic(
                    SYSTEM_BATTERY_CHAR_UUID,
                    BLECharacteristic::PROPERTY_READ
                  );

// INTERVAL
  pSystemService->createCharacteristic(
                    SYSTEM_INTERVAL_CHAR_UUID,
                    BLECharacteristic::PROPERTY_READ |
                    BLECharacteristic::PROPERTY_WRITE
                  );
  pSystemService->getCharacteristic(SYSTEM_INTERVAL_CHAR_UUID)->setCallbacks(new SystemIntervalCbk);

// TOKEN
  pSystemService->createCharacteristic(
                    SYSTEM_TOKEN_CHAR_UUID,
                    BLECharacteristic::PROPERTY_WRITE
                  );
  pSystemService->getCharacteristic(SYSTEM_TOKEN_CHAR_UUID)->setCallbacks(new SystemTokenCbk);

// RESET
  pSystemService->createCharacteristic(
                    SYSTEM_RESET_CHAR_UUID,
                    BLECharacteristic::PROPERTY_WRITE
                  );
  pSystemService->getCharacteristic(SYSTEM_RESET_CHAR_UUID)->setCallbacks(new SystemResetCbk);

// FIRMWARE VERSION
  pSystemService->createCharacteristic(
                    SYSTEM_FIRMWARE_VERSION_CHAR_UUID,
                    BLECharacteristic::PROPERTY_READ
                  );

// FIRMWARE UPDATE
  pSystemService->createCharacteristic(
                    SYSTEM_FIRMWARE_UPDATE_CHAR_UUID,
                    BLECharacteristic::PROPERTY_WRITE |
                    BLECharacteristic::PROPERTY_NOTIFY
                  );
  BLE2902 *pFirmwareUpdateNotifyDescriptor = new BLE2902();
  pSystemService->getCharacteristic(SYSTEM_FIRMWARE_UPDATE_CHAR_UUID)->addDescriptor(pFirmwareUpdateNotifyDescriptor);
  pSystemService->getCharacteristic(SYSTEM_FIRMWARE_UPDATE_CHAR_UUID)->setCallbacks(new SystemFirmwareUpdateCbk);

  ble::startService(pSystemService);

//
// PAIR
//
  BLEService *pPairService = ble::createService(PAIR_SERVICE_UUID, 8);

// BEGIN
  pPairService->createCharacteristic(
                  PAIR_BEGIN_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE
                );
  pPairService->getCharacteristic(PAIR_BEGIN_CHAR_UUID)->setCallbacks(new PairBeginCbk());

// SENSOR INFO
  pPairService->createCharacteristic(
                  PAIR_SENSOR_INFO_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_NOTIFY
                );
  BLE2902 *pSensorInfoNotifyDescriptor = new BLE2902();
  pPairService->getCharacteristic(PAIR_SENSOR_INFO_CHAR_UUID)->addDescriptor(pSensorInfoNotifyDescriptor);

// START CYCLE
  pPairService->createCharacteristic(
                  PAIR_START_CYCLE_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE_NR
                );
  pPairService->getCharacteristic(PAIR_START_CYCLE_CHAR_UUID)->setCallbacks(new PairStartCycleCbk());

  ble::startService(pPairService);
  
//
// WIFI
//
  BLEService *pWifiService =  ble::createService(WIFI_SERVICE_UUID, 10);

// SSID
  pWifiService->createCharacteristic(
                  WIFI_SSID_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_WRITE
                );
  pWifiService->getCharacteristic(WIFI_SSID_CHAR_UUID)->setCallbacks(new WifiSsidCbk);

// PASSWORD
  pWifiService->createCharacteristic(
                  WIFI_PASSWORD_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE
                );
  pWifiService->getCharacteristic(WIFI_PASSWORD_CHAR_UUID)->setCallbacks(new WifiPasswordCbk);

// SCAN START
  pWifiService->createCharacteristic(
                  WIFI_SCAN_START_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE
                );
  pWifiService->getCharacteristic(WIFI_SCAN_START_CHAR_UUID)->setCallbacks(new WifiScanStartCbk);

// SCAN RESULTS
  pWifiService->createCharacteristic(
                  WIFI_SCAN_RESULTS_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_NOTIFY
                );
  BLE2902 *pWifiScanResultsNotifyDescriptor = new BLE2902();
  pWifiService->getCharacteristic(WIFI_SCAN_RESULTS_CHAR_UUID)->addDescriptor(pWifiScanResultsNotifyDescriptor);

  ble::startService(pWifiService);

//
// SERVER
//
  BLEService *pServerService = ble::createService(SERVER_SERVICE_UUID, 5);

// API DOMAIN
  pServerService->createCharacteristic(
                  SERVER_API_DOMAIN_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_WRITE
                );
  pServerService->getCharacteristic(SERVER_API_DOMAIN_CHAR_UUID)->setCallbacks(new ServerAPIDomainCbk);

// STATIC DOMAIN
  pServerService->createCharacteristic(
                  SERVER_STATIC_DOMAIN_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_WRITE
                );
  pServerService->getCharacteristic(SERVER_STATIC_DOMAIN_CHAR_UUID)->setCallbacks(new ServerStaticDomainCbk);

  ble::startService(pServerService);

//
// TEST
//
  BLEService *pTestService = ble::createService(TEST_SERVICE_UUID, 6);

// TEST COMMAND
  pTestService->createCharacteristic(
                  TEST_COMMAND_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE
                );
  pTestService->getCharacteristic(TEST_COMMAND_CHAR_UUID)->setCallbacks(new TestCommandCbk);

// TEST RESULTS
  pTestService->createCharacteristic(
                  TEST_RESULT_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_NOTIFY
                );
  BLE2902 *pTestResultNotifyDescriptor = new BLE2902();
  pTestService->getCharacteristic(TEST_RESULT_CHAR_UUID)->addDescriptor(pTestResultNotifyDescriptor);

  ble::startService(pTestService);

//
// READINGS
//
  BLEService *pReadingsService = ble::createService(READINGS_SERVICE_UUID, 6);

// READINGS COMMAND
  pReadingsService->createCharacteristic(
                  READINGS_COMMAND_CHAR_UUID,
                  BLECharacteristic::PROPERTY_WRITE
                );
  pReadingsService->getCharacteristic(READINGS_COMMAND_CHAR_UUID)->setCallbacks(new ReadingsCommandCbk);

// READINGS DATA
  pReadingsService->createCharacteristic(
                  READINGS_DATA_CHAR_UUID,
                  BLECharacteristic::PROPERTY_READ |
                  BLECharacteristic::PROPERTY_NOTIFY
                );
  BLE2902 *pReadingsDataNotifyDescriptor = new BLE2902();
  pReadingsService->getCharacteristic(READINGS_DATA_CHAR_UUID)->addDescriptor(pReadingsDataNotifyDescriptor);

  ble::startService(pReadingsService);

  uint32_t now = rtc::getTime().unixtime();
  bleSetTime(now);
  bleSetBattery(gBattery);
  bleSetInterval(gInterval);
  bleSetWifiSsid(gWifiSsid);
  bleSetAPIDomain(gAPIDomain);
  bleSetStaticDomain(gStaticDomain);
  bleSetFirmwareVersion(gFirmwareVersion);
  bleSetTestResult(gTestResult);
}
