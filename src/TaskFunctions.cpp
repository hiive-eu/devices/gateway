#include "TaskFunctions.h"
#include <Defines.h>
#include "BLEConfig.h"
#include "Reading.h"
#include "SafeString.h"
#include "Storage.h"
#include <cstring>

createSafeString(TEST_DIR_PATH, 5, "/test");
createSafeString(TEST_FILE_PATH, 15, "/test/test_file");
createSafeString(FILE_WRITE_CONTENT, 15, "A place to bee!");
createSafeString(fileReadContent, 15);

int8_t bleTaskDoneOk = 1;
int8_t bleTaskDoneError = -1;

struct SendReadingTaskParams {
  char filename[8];
  BLECharacteristic* pReadingsDataChar;
};

struct DeleteReadingFileTaskParams {
  char filename[8];
  BLECharacteristic* pReadingsDataChar;
};

void scanWifi(BLECharacteristic *pWifiScanResultsChar) {
  xTaskCreate(
    scanWifiTask,
    "wifiScanTask",
    10000,
    (void*)pWifiScanResultsChar,
    1,
    NULL
  );
}

void testWifi(bool updateBLEChar) {
  xTaskCreate(
    testWifiTask,
    "testWifiTask",
    5000,    // Stack size (bytes)
    (void *) updateBLEChar,
    1,        // Task priority
    NULL
  );
}

void testSD(bool updateBLEChar) {
  xTaskCreate(
    testSDTask,
    "testSDTask",
    3000,    // Stack size (bytes)
    (void *)updateBLEChar,
    1,        // Task priority
    NULL
  );
}

void sendFilenames(BLECharacteristic *pReadingsDataChar) {
  xTaskCreate(
    sendFilenamesTask,
    "sendReadingTask",
    5000,    // Stack size (bytes)
    (void*)pReadingsDataChar,
    1,        // Task priority
    NULL
  );
}


void sendReading(SafeString &filename, BLECharacteristic *pReadingsDataChar) {
  SendReadingTaskParams* params = new SendReadingTaskParams;
  strcpy(params->filename, filename.c_str());
  params->pReadingsDataChar = pReadingsDataChar;
  xTaskCreate(
    sendReadingTask,
    "sendReadingTask",
    5000,    // Stack size (bytes)
    (void*)params,
    1,        // Task priority
    NULL
  );
}

void deleteReadingFile(SafeString &filename, BLECharacteristic *pReadingsDataChar) {
  DeleteReadingFileTaskParams* params = new DeleteReadingFileTaskParams;
  strcpy(params->filename, filename.c_str());
  params->pReadingsDataChar = pReadingsDataChar;
  xTaskCreate(
    deleteReadingFileTask,
    "deleteReadingFileTask",
    5000,    // Stack size (bytes)
    (void*)params,
    1,        // Task priority
    NULL
  );
}

void scanWifiTask(void *pvParameter) {
#if defined(DEBUG) && DEBUG > 0
  UBaseType_t uxHighWaterMark;  
#endif
  createSafeString(res, 39);
  BLECharacteristic *pWifiScanResultsChar = (BLECharacteristic*)pvParameter;
  int n = WiFi.scanNetworks();
  if (n < 0) {
      pWifiScanResultsChar->setValue((uint8_t*)&bleTaskDoneError, 1);
      pWifiScanResultsChar->notify();
      Serial.println(F("[WIFI]    WIFI scan error "));
      WiFi.scanDelete();
      vTaskDelete(NULL);
  }
  n = n > 50 ? 50 : n;
  Serial.println(F("[WIFI]    WIFI Networks: "));
  vTaskDelay(pdMS_TO_TICKS(1000)); // wait for app to listen
  for (int i = 0; i <= n; i++) {
    if (i == n) {
      pWifiScanResultsChar->setValue((uint8_t*)&bleTaskDoneOk, 1);
      pWifiScanResultsChar->notify();
    }
    res.clear();
    if (isValidWifiNetwork(WiFi.encryptionType(i))) {
      res = WiFi.SSID(i).c_str();
      res += F("#");
      res += WiFi.RSSI(i);
      res += F("#");
      res += (uint8_t)WiFi.encryptionType(i);
#if defined(DEBUG) && DEBUG > 0
      Serial.print(F("           - "));
      Serial.print(F(" - "));
      Serial.println(res);
#endif
      pWifiScanResultsChar->setValue(res.c_str());
      pWifiScanResultsChar->notify();
      vTaskDelay(pdMS_TO_TICKS(50));
    }
  }
#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("scanWifiTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  WiFi.scanDelete();
  vTaskDelete(NULL);
}

void testSDTask(void *pvParameter) {
  bool updateBLEChar = (bool) pvParameter;
#if defined(DEBUG) && DEBUG > 0
  UBaseType_t uxHighWaterMark;
#endif
  sd::enable();

  int8_t spaceUsed = sd::spaceUsed();
  if (spaceUsed < 0) {
    gTestResult.sd.res = -1;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }
  gTestResult.sd.spaceUsed = spaceUsed;

  if (!sd::exists(TEST_DIR_PATH)) {
    if (!sd::createDir(TEST_DIR_PATH)) {
      gTestResult.sd.res = -2;
      sd::disable();
      if (updateBLEChar) {
        bleSetTestResult(gTestResult);
      }
      bootProgress |= 0b01;
      vTaskDelete(NULL);
    }
  }

  if (!sd::createFile(TEST_FILE_PATH)) {
    gTestResult.sd.res = -3;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }

  if (!sd::writeFile(TEST_FILE_PATH, FILE_WRITE_CONTENT)) {
    gTestResult.sd.res = -4;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }
  
  if (!sd::readFile(TEST_FILE_PATH, fileReadContent)) {
    gTestResult.sd.res = -5;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }

  if (!sd::removeFile(TEST_FILE_PATH)) {
    gTestResult.sd.res = -6;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }

  if (!sd::removeDir(TEST_DIR_PATH)) {
    gTestResult.sd.res = -7;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }

  if (fileReadContent != FILE_WRITE_CONTENT) {
    gTestResult.sd.res = -8;
    sd::disable();
    if (updateBLEChar) {
      bleSetTestResult(gTestResult);
    }
    bootProgress |= 0b01;
    vTaskDelete(NULL);
  }

  gTestResult.sd.res = 1;
  sd::disable();
  if (updateBLEChar) {
    bleSetTestResult(gTestResult);
  }

#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("testSDTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  bootProgress |= 0b01;
  vTaskDelete(NULL);
}

void testWifiTask(void *pvParameter) {
  bool updateBLEChar = (bool) pvParameter;
#if defined(DEBUG) && DEBUG > 0
  UBaseType_t uxHighWaterMark;
#endif

  Serial.println("[WIFI TEST]    Enabling wifi");
  if (!wifi::enable(gWifiSsid, gWifiPassword)) {
  Serial.println("[WIFI TEST]    Failed to enable wifi");
      gTestResult.wifi.res = -1;
      wifi::disable();
      WiFi.disconnect(false);   // Needed for WiFi scan to work
      bootProgress |= 0b10;
      if (updateBLEChar) {
        bleSetTestResult(gTestResult);
      }
    vTaskDelete(NULL);
  }
  Serial.println("[WIFI TEST]    Wifi enabled");
  if (!wifi::isConnected()) {
    wifi::connect();
  }
  uint32_t connectBeginMillis = millis();
  while (!wifi::isConnected()) {
    Serial.println("[WIFI TEST]    Connecting wifi");
    vTaskDelay(pdMS_TO_TICKS(500));
    if (millis() - connectBeginMillis > 10000) {
      Serial.println("[WIFI TEST]    Connect timeout");
      gTestResult.wifi.res = -2;
      wifi::disable();
      WiFi.disconnect(false);   // Needed for WiFi scan to work
      bootProgress |= 0b10;
      if (updateBLEChar) {
        bleSetTestResult(gTestResult);
      }
      vTaskDelete(NULL);
    }
  }
  Serial.println("[WIFI TEST]    Connected");

  gTestResult.wifi.rssi = wifi::getRSSI();

  HTTPClient getCAHTTPClient;
#if defined(DEBUG) && DEBUG > 0
  getCAHTTPClient.setAuthorization(gDevAuthUser.c_str(), gDevAuthPassword.c_str());
#endif
  getCAHTTPClient.setUserAgent(gHTTPUserAgent.c_str());
  getCAHTTPClient.setConnectTimeout(HTTP_TIMEOUT_MS);
  getCAHTTPClient.setTimeout(HTTP_TIMEOUT_MS);
  getCAHTTPClient.begin(gStaticDomain.c_str(), 80, CACERT_PATH.c_str());
  int status = getCAHTTPClient.GET();
  String caCert = getCAHTTPClient.getString();
  getCAHTTPClient.end();
  Serial.print("[WIFI TEST]    Get CA status code: ");
  Serial.println(status);

  if (status != 200) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("[WIFI TEST]    failed to get CA");
#endif
    gTestResult.wifi.res = -3; // Failed to get CA
  } else {
  HTTPClient apiHTTPClient;
#if defined(DEBUG) && DEBUG > 0
    apiHTTPClient.setAuthorization(gDevAuthUser.c_str(), gDevAuthPassword.c_str());
#endif
    apiHTTPClient.setConnectTimeout(HTTP_TIMEOUT_MS);
    apiHTTPClient.setTimeout(HTTP_TIMEOUT_MS);
    apiHTTPClient.begin(gAPIDomain.c_str(), 443, TEST_PATH.c_str(), caCert.c_str());
    apiHTTPClient.addHeader("Content-Type", "application/json");
    apiHTTPClient.addHeader("Token", gToken.c_str());
    createSafeString(testMACJson, 20);
    testMACJson += "{\"m\":\"";
    testMACJson += gMacAscii;
    testMACJson += "\"}";
    status = apiHTTPClient.POST(testMACJson.c_str());
    apiHTTPClient.end();
    Serial.print("[WIFI TEST]    Test GW auth status code: ");
    Serial.println(status);
    if (status != 204) {
      gTestResult.wifi.res = -4; // Failed to make SSL request
    } else {
      gCACert = caCert.c_str();
      storageWriteCACert(caCert);
      gTestResult.wifi.res = 1;
    }
  }
  wifi::disable();
  WiFi.disconnect(false);   // Needed for WiFi scan to work
  if (updateBLEChar) {
    bleSetTestResult(gTestResult);
  }

#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("testWifiTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  bootProgress |= 0b10;
  vTaskDelete(NULL);
}

void sendFilenamesTask(void *pvParameter) {
  UBaseType_t uxHighWaterMark;
  BLECharacteristic *pReadingsDataChar = (BLECharacteristic*)pvParameter;

  createSafeString(readingFilenames, 1000); // ~10 years of reading files
  createSafeString(readingFilename, 7);
  int8_t numFiles = listReadingFiles(readingFilenames);
  if (numFiles < 0) {
#if defined(DEBUG) && DEBUG > 0
    Serial.println("failed to get filenames");
    pReadingsDataChar->setValue((uint8_t*)&bleTaskDoneOk, 1);
    pReadingsDataChar->notify();
    vTaskDelete(NULL);
#endif
  }
  vTaskDelay(pdMS_TO_TICKS(500)); // wait for app to listen
#if defined(DEBUG) && DEBUG > 0
  Serial.println(F("[BLE]     Filenames: "));
#endif
  for (int i = 0; i < numFiles; i++) {
    readingFilename.clear();
    readingFilename.readFrom(readingFilenames, i*READING_FILE_NAME_SIZE);
#if defined(DEBUG) && DEBUG > 0
    Serial.print(F("          - "));
    Serial.println(readingFilename);
#endif
    pReadingsDataChar->setValue(readingFilename.c_str());
    pReadingsDataChar->notify();
    vTaskDelay(pdMS_TO_TICKS(50));
  }
  pReadingsDataChar->setValue((uint8_t*)&bleTaskDoneOk, 1);
  pReadingsDataChar->notify();

#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("sendFilenamesTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  vTaskDelete(NULL);
}

void sendReadingTask(void *pvParameter) {
  UBaseType_t uxHighWaterMark;
  SendReadingTaskParams* params = (SendReadingTaskParams*)pvParameter;
  BLECharacteristic *pReadingsDataChar = params->pReadingsDataChar;
  gBytesReceivedReadingTransfer = -1;

  vTaskDelay(pdMS_TO_TICKS(500)); // wait for app to listen
  File file = getReadingFileHandle(params->filename);
  if (!file) {
#if defined(DEBUG) && DEBUG > 0
    Serial.print(F("[BLE]     Sending file: "));
    Serial.println(params->filename);
#endif
    pReadingsDataChar->setValue((uint8_t*)&bleTaskDoneError, 1);
    pReadingsDataChar->notify();
    vTaskDelete(NULL);
  }

  uint32_t filesize = file.size();
  int32_t lastBytesReceived = gBytesReceivedReadingTransfer;
  pReadingsDataChar->setValue((uint8_t*)&filesize, 4);
  pReadingsDataChar->notify();

#if defined(DEBUG) && DEBUG > 0
    Serial.print(F("File send progress: "));
    Serial.print(0);
    Serial.print(F("/"));
    Serial.println(filesize);
#endif 

  uint16_t bytesPerPacket = gMtu-3; // TODO: Replace with actual MTU
  uint16_t bytesToRead = bytesPerPacket;
  char buffer[bytesPerPacket];
  uint32_t timeoutBegin;;    
  bool timeout = false;
  while (gBytesReceivedReadingTransfer != filesize) {
    timeoutBegin = millis();    
    while (lastBytesReceived == gBytesReceivedReadingTransfer) {
      if (millis() - timeoutBegin > 5000) {
        timeout = true;
        break;
      }
      vTaskDelay(pdMS_TO_TICKS(5));
    }

    if (timeout) {
      break;
    }

    if (gBytesReceivedReadingTransfer != filesize) {
      lastBytesReceived = gBytesReceivedReadingTransfer;
      if (gBytesReceivedReadingTransfer+bytesPerPacket > filesize) {
        bytesToRead = filesize - gBytesReceivedReadingTransfer;
      }
      file.seek(gBytesReceivedReadingTransfer);
      file.readBytes(buffer, bytesToRead);
      pReadingsDataChar->setValue((uint8_t*)buffer, bytesToRead);
      pReadingsDataChar->notify();
#if defined(DEBUG) && DEBUG > 0
      Serial.print("File send progress: ");
      Serial.print(gBytesReceivedReadingTransfer+bytesToRead);
      Serial.print("/");
      Serial.println(filesize);
#endif
    }
  }
  if (timeout) {
    pReadingsDataChar->setValue((uint8_t*)&bleTaskDoneError, 1);
    Serial.println("SEND READING TIMEOUT");
  } else {
    pReadingsDataChar->setValue((uint8_t*)&bleTaskDoneOk, 1);
    Serial.println("SEND READING DONE");
  }
  pReadingsDataChar->notify();
#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("sendReadingTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  vTaskDelete(NULL);
}

void deleteReadingFileTask(void *pvParameter) {
  UBaseType_t uxHighWaterMark;
  DeleteReadingFileTaskParams* params = (DeleteReadingFileTaskParams*)pvParameter;
  BLECharacteristic *pReadingsDataChar = params->pReadingsDataChar;

  Serial.print("[BLE]     Removing file: ");
  Serial.println(params->filename);
  uint8_t res = (uint8_t)deleteReadingFile(params->filename);
  pReadingsDataChar->setValue(&res, 1);
  pReadingsDataChar->notify();
  
#if defined(DEBUG) && DEBUG > 0
  uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
  Serial.print("deleteReadingFileTask mem: ");
  Serial.println(uxHighWaterMark);
#endif
  vTaskDelete(NULL);
}

bool isValidWifiNetwork(wifi_auth_mode_t authMode) {
  switch (authMode) {
    case WIFI_AUTH_WPA2_ENTERPRISE:
      return false;
      break;
    default:
      return true;
      break;
  }
}

