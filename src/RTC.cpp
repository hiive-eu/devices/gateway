#include "RTC.h"
#include "Hardware.h"
#include "Utils.h"

namespace rtc {
  MCP7940_Class mcp7940_internal;  

  bool init() {
    uint8_t counter = 0;
    while(!mcp7940_internal.begin()) {
      counter += 1;
      if (counter < 3) {
        delay(100);
      } else {
        return false;
      }
    }
  
    mcp7940_internal.clearPowerFail();
    while (!mcp7940_internal.deviceStatus()) {
      counter = 0;
      if (!mcp7940_internal.deviceStart()) {
        counter += 1;
        if (counter < 3) {
          delay(100);
        } else {
          return false;
        }
      }
    }
  
    return true;
  }
  
  void clearAlarms() {
    clearAlarm0();
    clearAlarm1();
  };
  DateTime getTime() {
    return mcp7940_internal.now();
  };
  bool getAlarm0() {
    return mcp7940_internal.isAlarm(0);
  };
  bool getAlarm0State() {
    return mcp7940_internal.getAlarmState(0);
  }
  bool getAlarm1State() {
    return mcp7940_internal.getAlarmState(1);
  }
  DateTime getAlarmTime0() {
    uint8_t alarmType;
    DateTime time = mcp7940_internal.getAlarm(0, alarmType);
    return time;
  };
  bool getAlarm1() {
    return mcp7940_internal.isAlarm(1);
  };
  DateTime getAlarmTime1() {
    uint8_t alarmType;
    DateTime time = mcp7940_internal.getAlarm(1, alarmType);
    return time;
  };
  uint8_t getMFP() {
    return mcp7940_internal.getMFP();
  }
  void setAlarm0(DateTime time) {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Set alarm0"));
    printDate(time);
  #endif
    mcp7940_internal.setAlarm(0, matchAll, time, 1);
  };
  void setAlarm1(DateTime time) {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Set alarm1"));
  #endif
    mcp7940_internal.setAlarm(1, matchAll, time, 1);
  };
  void setTime(DateTime time) {
    mcp7940_internal.calibrateOrAdjust(time);
  }
  void clearAlarm0() {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Clear alarm0"));
  #endif
    mcp7940_internal.clearAlarm(0);
  };
  void clearAlarm1() {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Clear alarm1"));
  #endif
    mcp7940_internal.clearAlarm(1);
  };
  void disableAlarm0() {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Disable alarm0"));
  #endif
    mcp7940_internal.setAlarmState(0, false);
  };
  void disableAlarm1() {
  #if defined(DEBUG) && DEBUG > 0
    Serial.println(F("[RTC]     Disable alarm1"));
  #endif
    mcp7940_internal.setAlarmState(1, false);
  };
  void setMFP(bool v) {
    bool success = mcp7940_internal.setMFP(v);
  #if defined(DEBUG) && DEBUG > 0
    if (success) {
      Serial.printf("[RTC]     MFP -> %d\r\n", v);
    } else {
      Serial.printf("[RTC]     Error seeting MFP -> %d\r\n", v);
    }
  #endif
  }
  void setPolarity(bool v) {
  #if defined(DEBUG) && DEBUG > 0
    Serial.printf("[RTC]     Polarity -> %d\r\n", v);
  #endif
    mcp7940_internal.setAlarmPolarity(v);
  }
  
  void waitUntil(DateTime time) {
    uint64_t seconds = (time - getTime()).totalseconds();
    delay(seconds*1000);
  };
}
