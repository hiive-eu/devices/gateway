#include "Sensors.h"

RTC_DATA_ATTR PairedSensor pairedSensors[MAX_SENSORS] = {};

void clearPairedSensors() {
  for (int i = 0; i < MAX_SENSORS; i++) {
    pairedSensors[i].mac = 0;
    pairedSensors[i].readingReceived = false;
  }
}

uint8_t getNumPairedSensors() {
  for (int i = 0; i < MAX_SENSORS; i++) {
    if (pairedSensors[i].mac == 0) {
      return i;
    }
  }
  return MAX_SENSORS;
}

uint8_t getPairedSensorIndex(uint64_t sensor) {
  for (int i = 0; i < MAX_SENSORS; i++) {
    if (pairedSensors[i].mac == sensor) {
      return i;
    }
  }
  return -1;
}

uint8_t newPairedSensor(uint64_t sensor) {
  uint8_t numPairedSensors = getNumPairedSensors();
  pairedSensors[numPairedSensors].mac = sensor;
  return numPairedSensors;
}

void setPairedSensorReadingReceived(uint64_t sensor, bool received) {
  uint8_t i = getPairedSensorIndex(sensor);
  pairedSensors[i].readingReceived = received;
}
