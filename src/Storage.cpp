#include "Storage.h"
#include "SafeString.h"

Preferences preferences;

createSafeString(STORAGE_NAMESPACE, 7, "storage");

createSafeString(STORAGE_KEY_INITIALIZED, 11, "initialized");
createSafeString(STORAGE_KEY_INITIALIZED_AFTER_UPDATE, 10, "initupdate");
createSafeString(STORAGE_KEY_SHIPPING_DISABLED, 8, "shipping");
createSafeString(STORAGE_KEY_TOKEN, 5, "token");
createSafeString(STORAGE_KEY_CACERT, 6, "cacert");
createSafeString(STORAGE_KEY_INTERVAL, 8, "interval");
createSafeString(STORAGE_KEY_WIFI_SSID, 4, "ssid");
createSafeString(STORAGE_KEY_WIFI_PASSWORD, 8, "password");
createSafeString(STORAGE_KEY_API_DOMAIN, 9, "apidomain");
createSafeString(STORAGE_KEY_STATIC_DOMAIN, 12, "staticdomain");

constexpr bool STORAGE_DEFAULT_INITIALIZED = false;
constexpr bool STORAGE_DEFAULT_INITIALIZED_AFTER_UPDATE = false;
constexpr bool STORAGE_DEFAULT_SHIPPING_DISABLED = false;
createSafeString(STORAGE_DEFAULT_TOKEN, 0, "");
createSafeString(STORAGE_DEFAULT_CACERT, 0, "");
constexpr uint8_t STORAGE_DEFAULT_INTERVAL = 20;
createSafeString(STORAGE_DEFAULT_WIFI_SSID, 0, "");
createSafeString(STORAGE_DEFAULT_WIFI_PASSWORD, 0, "");
#if defined(DEBUG) && DEBUG > 0
createSafeString(STORAGE_DEFAULT_API_DOMAIN, 23, "dev.readings.hiive.buzz");
createSafeString(STORAGE_DEFAULT_STATIC_DOMAIN, 23, "dev.releases.hiive.buzz");
#else
createSafeString(STORAGE_DEFAULT_API_DOMAIN, 19, "readings.hiive.buzz");
createSafeString(STORAGE_DEFAULT_STATIC_DOMAIN, 19, "releases.hiive.buzz");
#endif

bool storageReadInitialized() {
  bool initialized;
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), false)) {
    initialized = STORAGE_DEFAULT_INITIALIZED;
  } else if (preferences.isKey(STORAGE_KEY_INITIALIZED.c_str())) {
    initialized = preferences.getBool(STORAGE_KEY_INITIALIZED.c_str(), STORAGE_DEFAULT_INITIALIZED);
    preferences.end();
  } else {
    initialized = STORAGE_DEFAULT_INITIALIZED;
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Initialized <- %d\r\n", initialized);
#endif
  return initialized;
}

bool storageReadInitializedAfterUpdate() {
  bool initializedAfterUpdate;
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), false)) {
    initializedAfterUpdate = STORAGE_DEFAULT_INITIALIZED_AFTER_UPDATE;
  } else if (preferences.isKey(STORAGE_KEY_INITIALIZED_AFTER_UPDATE.c_str())){
    initializedAfterUpdate = preferences.getBool(STORAGE_KEY_INITIALIZED_AFTER_UPDATE.c_str(), STORAGE_DEFAULT_INITIALIZED_AFTER_UPDATE);
    preferences.end();
  } else {
    initializedAfterUpdate = STORAGE_DEFAULT_INITIALIZED_AFTER_UPDATE;
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] InitializedAfterUpdate <- %d\r\n", initializedAfterUpdate);
#endif
  return initializedAfterUpdate;
}

bool storageReadShippingDisabled() {
  bool shippingDisabled;
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), false)) {
    shippingDisabled = STORAGE_DEFAULT_SHIPPING_DISABLED;
  } else if (preferences.isKey(STORAGE_KEY_SHIPPING_DISABLED.c_str())){
    shippingDisabled = preferences.getBool(STORAGE_KEY_SHIPPING_DISABLED.c_str(), STORAGE_DEFAULT_SHIPPING_DISABLED);
    preferences.end();
  } else {
    shippingDisabled = STORAGE_DEFAULT_SHIPPING_DISABLED;
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Shipping disabled <- %d\r\n", shippingDisabled);
#endif
  return shippingDisabled;
}

void storageReadToken(SafeString &s){
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_TOKEN.c_str();
  } else if (preferences.isKey(STORAGE_KEY_TOKEN.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_TOKEN.c_str(), STORAGE_DEFAULT_TOKEN.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_TOKEN.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Token <- %s\r\n", s.c_str());
#endif
}

uint8_t storageReadInterval() {
  uint8_t interval;
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    interval = STORAGE_DEFAULT_INTERVAL;
  } else if (preferences.isKey(STORAGE_KEY_INTERVAL.c_str())) {
    interval = preferences.getUChar(STORAGE_KEY_INTERVAL.c_str(), STORAGE_DEFAULT_INTERVAL);
    preferences.end();
  } else {
    interval = STORAGE_DEFAULT_INTERVAL;
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Interval <- %d\r\n", interval);
#endif
  return interval;
}
void storageReadCACert(SafeString &s) {
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_CACERT.c_str();
  } else if (preferences.isKey(STORAGE_KEY_CACERT.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_CACERT.c_str(), STORAGE_DEFAULT_CACERT.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_CACERT.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] CACert <- %s\r\n", s.c_str());
#endif
}
void storageReadWifiSsid(SafeString &s) {
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_WIFI_SSID.c_str();
  } else if (preferences.isKey(STORAGE_KEY_WIFI_SSID.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_WIFI_SSID.c_str(), STORAGE_DEFAULT_WIFI_SSID.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_WIFI_SSID.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Wifi ssid <- %s\r\n", s.c_str());
#endif
}
void storageReadWifiPassword(SafeString &s) {
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_WIFI_PASSWORD.c_str();
  } else if (preferences.isKey(STORAGE_KEY_WIFI_PASSWORD.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_WIFI_PASSWORD.c_str(), STORAGE_DEFAULT_WIFI_PASSWORD.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_WIFI_PASSWORD.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] WiFi password <- %s\r\n", s.c_str());
#endif
}
void storageReadAPIDomain(SafeString &s) {
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_API_DOMAIN.c_str();
  } else if (preferences.isKey(STORAGE_KEY_API_DOMAIN.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_API_DOMAIN.c_str(), STORAGE_DEFAULT_API_DOMAIN.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_API_DOMAIN.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Server domain <- %s\r\n", s.c_str());
#endif
}
void storageReadStaticDomain(SafeString &s) {
  s.clear();
  if (!preferences.begin(STORAGE_NAMESPACE.c_str(), true)) {
    s = STORAGE_DEFAULT_STATIC_DOMAIN.c_str();
  } else if (preferences.isKey(STORAGE_KEY_STATIC_DOMAIN.c_str())) {
    String tmp = preferences.getString(STORAGE_KEY_STATIC_DOMAIN.c_str(), STORAGE_DEFAULT_STATIC_DOMAIN.c_str());
    preferences.end();
    s = tmp.c_str();
  } else {
    s = STORAGE_DEFAULT_STATIC_DOMAIN.c_str();
  }
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Server domain <- %s\r\n", s.c_str());
#endif
}
void storageWriteInitialized(bool initialized) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putBool(STORAGE_KEY_INITIALIZED.c_str(), initialized);
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] initialized -> %d\r\n", initialized);
#endif
}

void storageWriteInitializedAfterUpdate(bool initializedAfterUpdate) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putBool(STORAGE_KEY_INITIALIZED_AFTER_UPDATE.c_str(), initializedAfterUpdate);
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] initializedAfterUpdate -> %d\r\n", initializedAfterUpdate);
#endif
}

void storageWriteShippingDisabled(bool shippingDisabled) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putBool(STORAGE_KEY_SHIPPING_DISABLED.c_str(), shippingDisabled);
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Shipping disabled -> %d\r\n", shippingDisabled);
#endif
}

void storageWriteToken(SafeString &token) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_TOKEN.c_str(), token.c_str());
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Token -> %s\r\n", token.c_str());
#endif
}

void storageWriteInterval(uint8_t interval) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putUChar(STORAGE_KEY_INTERVAL.c_str(), interval);
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Interval -> %d\r\n", interval);
#endif
}

void storageWriteCACert(String CACert) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_CACERT.c_str(), CACert);
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] CACert -> %s\r\n", CACert.c_str());
#endif
}
void storageWriteWifiSsid(SafeString &ssid) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_WIFI_SSID.c_str(), ssid.c_str());
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Wifi SSID -> %s\r\n", ssid.c_str());
#endif
}

void storageWriteWifiPassword(SafeString &password) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_WIFI_PASSWORD.c_str(), password.c_str());
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Wifi password -> %s\r\n", password.c_str());
#endif
}

void storageWriteAPIDomain(SafeString &domain) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_API_DOMAIN.c_str(), domain.c_str());
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] API domain -> %s\r\n", domain.c_str());
#endif
}

void storageWriteStaticDomain(SafeString &domain) {
  preferences.begin(STORAGE_NAMESPACE.c_str(), false);
  preferences.putString(STORAGE_KEY_STATIC_DOMAIN.c_str(), domain.c_str());
  preferences.end();
#if defined(DEBUG) && DEBUG > 0
  Serial.printf("[STORAGE] Static domain -> %s\r\n", domain.c_str());
#endif
}

void storageResetDefaults() {
  //storageWriteInitializedAfterUpdate(STORAGE_DEFAULT_INITIALIZED_AFTER_UPDATE);
  //storageWriteShippigDisabled(STORAGE_DEFAULT_SHIPPING_DISABLED);
  storageWriteToken(STORAGE_DEFAULT_TOKEN);
  storageWriteInterval(STORAGE_DEFAULT_INTERVAL);
  storageWriteWifiSsid(STORAGE_DEFAULT_WIFI_SSID);
  storageWriteWifiPassword(STORAGE_DEFAULT_WIFI_PASSWORD);
  storageWriteAPIDomain(STORAGE_DEFAULT_API_DOMAIN);
  storageWriteStaticDomain(STORAGE_DEFAULT_STATIC_DOMAIN);
}
