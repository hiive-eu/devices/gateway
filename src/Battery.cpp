#include "Battery.h"

namespace battery {
  void enable() {
    pinMode(PIN_DOUT_ACT_VBAT, OUTPUT);
    digitalWrite(PIN_DOUT_ACT_VBAT, 0x00);
    for (int i=0; i<BATTERY_WARMUP_SAMPLES; i++) {
      int reading = analogRead(PIN_AIN_VBAT);
    }
  }
  
  void disable() {
    pinMode(PIN_DOUT_ACT_VBAT, INPUT);
    digitalWrite(PIN_DOUT_ACT_VBAT, 0x01);
  }
  
  uint16_t readVoltage() {
    // First value is always wrong so we burn it
    int burned = analogRead(PIN_AIN_VBAT);
    uint32_t bat_reads = 0;
    for (int i = 0; i<BATTERY_SAMPLES; i++) {
      int reading = analogRead(PIN_AIN_VBAT);
  
      reading = reading - 77;
  
      double rLevel = (-0.000000000000016 * pow(reading,4)
          + 0.000000000118171 * pow(reading,3)
          - 0.000000301211691 * pow(reading,2)
          + 0.001109019271794 * reading
          + 0.034143524634089) * 1000;
      uint16_t voltage = (rLevel*(VBAT_R55+VBAT_R54))/VBAT_R55;
      bat_reads += voltage;
    }
    return (int)((float)bat_reads / BATTERY_SAMPLES);
  }
  
  uint8_t readPercentage() {
    uint16_t voltage = readVoltage();
    return batteryVoltageToPercentage(voltage);
  }
  
  uint8_t batteryVoltageToPercentage(uint16_t voltage) {
    if (voltage < VBAT_MIN) {voltage = VBAT_MIN;}
    if (voltage > VBAT_MAX) {voltage = VBAT_MAX;}
    return map(voltage, VBAT_MIN, VBAT_MAX, 0, 100);
  }
}
