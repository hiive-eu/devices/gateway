#include "WiFiControl.h"

namespace wifi {
  bool enabled = false;

  WiFiEventId_t _externalLostConEvent;
  WiFiEventId_t _externalGotIpEvent;
  WiFiEventId_t _internalLostConEvent;
  char _ssid[33];
  char _password[65];

  bool enable(
    SafeString &ssid,
    SafeString &pass,
    void (*onLostConFunc)(WiFiEvent_t event, WiFiEventInfo_t info),
    void (*onGotIpFunc)(WiFiEvent_t event, WiFiEventInfo_t info)
  ) {
    _externalGotIpEvent = WiFi.onEvent(onGotIpFunc, ARDUINO_EVENT_WIFI_STA_GOT_IP);
    _externalLostConEvent = WiFi.onEvent(onLostConFunc, ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
  
    return enable(ssid, pass);
  }
  
  bool enable(
    SafeString &ssid,
    SafeString &pass
  ) {
    _internalLostConEvent = WiFi.onEvent([](WiFiEvent_t event, WiFiEventInfo_t info){connect();}, ARDUINO_EVENT_WIFI_STA_DISCONNECTED);
  
    if (ssid.length() > 32) {
  #if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[WIFI]    SSID too long"));
  #endif
      enabled = false;
    } else if (ssid.isEmpty()) {
  #if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[WIFI]    SSID empty"));
  #endif
      enabled = false;
    } else if (pass.length() > 64) {
  #if defined(DEBUG) && DEBUG > 0
      Serial.println(F("[WIFI]    Password too long"));
  #endif
      enabled = false;
    } else {
      createSafeStringFromCharArray(safeSsid, _ssid);
      safeSsid = ssid; // Sets private ssid
      createSafeStringFromCharArray(safePassword, _password);
      safePassword = pass; // sets private password
      enabled = true;
    }
  
    return enabled;
  }
  
  void disable() {
    if (_externalGotIpEvent) {
      WiFi.removeEvent(_externalGotIpEvent);
    }
    if (_externalLostConEvent) {
      WiFi.removeEvent(_externalLostConEvent);
    }
    WiFi.removeEvent(_internalLostConEvent);
    WiFi.disconnect(true, true);
    WiFi.mode(WIFI_OFF);
    
    enabled = false;
  }
  
  void connect() {
    if (_password[0] == '\0') {
      WiFi.begin(_ssid);
    } else {
      WiFi.begin(_ssid, _password);
    }
  }
  
  void disconnect() {
    WiFi.disconnect(false, false);
  }
  bool isConnected() {
    return WiFi.isConnected();
  }
  int8_t getRSSI() {
    return WiFi.RSSI();
  }
}
