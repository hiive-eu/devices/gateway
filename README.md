# *** NOT PRODUCTION READY ***

Known shortcomings:

* Make sure that licenses of used packages are not violated
* Device needs to be certified
* Check md5 sum of downloaded firmware before updating
* Implement BLE authentication, so only the owner can configure the gateway

Unknown shortcomings: most likely.

# Configuration and building

The firmware is built using PlatformIO and comes in two different variations. `dev` and `prod`.

To flash the gateway run the following command where `<ENV>` is replaced with either `dev` or `prod`:

```
pio run -e <ENV> --target upload --upload-port=/dev/ttyUSB0
```

Connect to serial port using `picocom`:

```
picocom -b 115200 /dev/ttyUSB0 --imap lfcrlf | ts "[%H:%M:%.S]"
```

There are helper scripts in the `scripts` directory.

The development variant requires HTTP basic auth parameters to access the API, it is passed during compilation using the follow environment variables:

`HIIVE_HTTP_AUTH_USER`

`HIIVE_HTTP_AUTH_PASS`

Parameters for configuring LoRa is set in `platformio.ini`

# Radio power configurations

The transmission power of BLE, WiFi and LoRa are all configured very conservatively, to ensure that the devices comply to the regulations related to
transmission power of devices.

# Shipping mode

The sensor will go into shipping mode (battery cut-off) on its first run. Shipping mode is disable by connecting the sensor over USB.
