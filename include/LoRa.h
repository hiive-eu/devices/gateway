#ifndef LORA_H
#define LORA_H

#include <Arduino.h>
#include <RadioLib.h>

#include "Defines.h"

namespace lora {
  constexpr uint16_t TX_TIMEOUT_MS = 500;
  constexpr uint16_t RX_TIMEOUT_PER_INTERVAL_MINUTE_MS = 200;
  
  constexpr uint16_t MAX_PACKET_LENGTH = 240;

  extern volatile LoRaState state;
  extern SX1262 radio;
  extern uint32_t sendStartMillis;
  extern uint32_t receiveStartMillis;
  extern uint16_t currentReceiveTimeout;

  void internalOnTxDone();
  void internalOnRxDone();
  
  int16_t on();
  int16_t off();
  int16_t standby();
  int16_t send(uint8_t* data, uint8_t length);
  int16_t receive(uint16_t timeout = 0);
}

#endif
