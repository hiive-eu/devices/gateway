#ifndef STORAGE_H
#define STORAGE_H

#include <Arduino.h>
#include <Preferences.h>
#include <SafeString.h>

//void storageInitialize();

bool storageReadInitialized();
bool storageReadInitializedAfterUpdate();
bool storageReadShippingDisabled();
void storageReadToken(SafeString &s);
uint8_t storageReadInterval();
void storageReadWifiSsid(SafeString &s);
void storageReadWifiPassword(SafeString &s);
void storageReadAPIDomain(SafeString &s);
void storageReadStaticDomain(SafeString &s);
void storageReadCACert(SafeString &s);

void storageWriteInitialized(bool initialized);
void storageWriteInitializedAfterUpdate(bool initializedAfterUpdate);
void storageWriteShippingDisabled(bool shippingDisabled);
void storageWriteToken(SafeString &token);
void storageWriteInterval(uint8_t interval);
void storageWriteWifiSsid(SafeString &ssid);
void storageWriteWifiPassword(SafeString &password);
void storageWriteAPIDomain(SafeString &domain);
void storageWriteStaticDomain(SafeString &domain);
void storageWriteCACert(String caCert);

void storageResetDefaults();

#endif
