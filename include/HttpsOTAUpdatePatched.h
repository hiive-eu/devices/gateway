#ifndef HTTPSOTAUPDATE_H
#define HTTPSOTAUPDATE_H
#include "esp_http_client.h"
#define HttpEvent_t esp_http_client_event_t

typedef enum
{
    HTTPS_OTA_IDLE,
    HTTPS_OTA_UPDATING,
    HTTPS_OTA_SUCCESS,
    HTTPS_OTA_FAIL,
    HTTPS_OTA_ERR
}HttpsOTAStatus_t;

class HttpsOTAUpdateClass {

    public:
    void begin(const char *url, const char *cert_pem, const char *user_agent, uint32_t timeout, bool skip_cert_common_name_check = false);
    void begin(const char *url, const char *cert_pem, const char *user_agent, uint32_t timeout, const char *http_auth_username, const char *http_auth_password, bool skip_cert_common_name_check = false);
    void onHttpEvent(void (*http_event_cb_t)(HttpEvent_t *));
    HttpsOTAStatus_t status();
};

extern HttpsOTAUpdateClass HttpsOTA;
#endif
