#ifndef BLECONTROL_H
#define BLECONTROL_H

#include "esp_gatts_api.h"
#include <Arduino.h>
#include <SafeString.h>
#include <BLEServer.h>
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <esp_bt.h>

namespace ble {
  extern BLEServer* pServer;

  void init(
    SafeString &name,
    void (*onConnectFunc)(BLEServer *pServer),
    void (*onDisconnectFunc)(BLEServer *pServer),
    void (*onMtuChangedFunc)(BLEServer *pServer, esp_ble_gatts_cb_param_t *param)
  );
  void start();
  void stop();
  BLEService* createService(BLEUUID uuid, uint32_t numHandles);
  void startService(BLEService *pService);

class BLEServerCbks: public BLEServerCallbacks {
  public:
    BLEServerCbks(
        void (*onConnectFunc)(BLEServer*),
        void (*onDisconnectFunc)(BLEServer*),
        void (*onMtuChangedFunc)(BLEServer*, esp_ble_gatts_cb_param_t*)
    )
      : onConnectFunc(onConnectFunc),
        onDisconnectFunc(onDisconnectFunc),
        onMtuChangedFunc(onMtuChangedFunc) { }

    void onDisconnect(BLEServer* pServer) {
      if (this->onDisconnectFunc) { this->onDisconnectFunc(pServer); }
      pServer->getAdvertising()->start();
    }

    void onConnect(BLEServer* pServer) {
      if (this->onConnectFunc) { this->onConnectFunc(pServer); }
    }

    void onMtuChanged(BLEServer* pServer, esp_ble_gatts_cb_param_t* param) {
      if (this->onMtuChangedFunc) { this->onMtuChangedFunc(pServer, param); }
    }
    void (*onDisconnectFunc)(BLEServer*);
    void (*onConnectFunc)(BLEServer*);
    void (*onMtuChangedFunc)(BLEServer*, esp_ble_gatts_cb_param_t*);
};

}
#endif
