#ifndef _SENSORS_H_
#define _SENSORS_H_

#include <Arduino.h>
#include "Defines.h"

struct PairedSensor {
  uint64_t mac = 0;
  bool readingReceived = false;
};

void clearPairedSensors();
uint8_t getNumPairedSensors();
uint8_t getPairedSensorIndex(uint64_t sensor);
uint8_t newPairedSensor(uint64_t sensor);
void setPairedSensorReadingReceived(uint64_t sensor, bool received);
#endif
