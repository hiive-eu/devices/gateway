#ifndef MAX_SENSORS
#define MAX_SENSORS 10
#endif

#ifndef FIRMWARE_VERSION
#define FIRMWARE_VERSION "0.1"
#endif

#ifndef DEFINES_H
#define DEFINES_H

enum LoRaState {
  LORA_STANDBY_S,
  LORA_START_SEND_S,
  LORA_SENDING_S,
  LORA_SENT_S,
  LORA_START_RECEIVE_S,
  LORA_RECEIVING_S,
  LORA_RECEIVED_S
};

enum FunctionState {
  BUTTON_DELAY_S,
  CONFIG_S,
  GATEWAY_S,
};

enum ConfigState {
  BOOTING_CS,
  BLE_CS,
  PAIR_CS,
  UPDATE_CS,
  SLEEP_CS
};

enum GatewayState {
  RECEIVE_GS,
  PROCESS_READINGS_GS,
  NTP_GS,
  SLEEP_GS
};

#endif

