#ifndef SLEEP_H
#define SLEEP_H

#include <Arduino.h>

#include "MCP7940.h"
#include "Utils.h"

namespace rtc {
  enum alarmTypes {
    matchSeconds,
    matchMinutes,
    matchHours,
    matchDayOfWeek,
    matchDayOfMonth,
    Unused1,
    Unused2,
    matchAll,
    Unknown
  };
  
  enum squareWaveTypes { Hz1, kHz4, kHz8, kHz32, Hz64 };
  
  bool init();
  void clearAlarms();
  DateTime getTime();
  bool getAlarm0();
  bool getAlarm0State();
  DateTime getAlarmTime0();
  bool getAlarm1();
  bool getAlarm1State();
  DateTime getAlarmTime1();
  uint8_t getMFP();
  void setAlarm0(DateTime time);
  void setAlarm1(DateTime time);
  void setTime(DateTime time);
  void clearAlarm0();
  void clearAlarm1();
  void disableAlarm0();
  void disableAlarm1();
  void setMFP(bool v);
  void setPolarity(bool v);
  void waitUntil(DateTime time);
}
#endif
