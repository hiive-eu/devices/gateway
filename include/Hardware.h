#ifndef HARDWARE_H
#define HARDWARE_H

#include <Arduino.h>

constexpr uint8_t PIN_DOUT_VEXT = Vext;

constexpr uint8_t PIN_WAKEUP_EXT1 = 16;

constexpr uint8_t PIN_AIN_VBAT = 1;
constexpr uint8_t PIN_DOUT_ACT_VBAT = 19;

constexpr uint8_t PIN_PWM_RGB_R = 18;
constexpr uint8_t PIN_PWM_RGB_G = 17;
constexpr uint8_t PIN_PWM_RGB_B = 20;

constexpr uint8_t PIN_MP2667_ISR_SHIPPING = 21;
constexpr uint8_t I2C_ADDR_MP2667 = 0x09;

constexpr uint8_t PIN_SD_SCK = 34;
constexpr uint8_t PIN_SD_MOSI = 7;
constexpr uint8_t PIN_SD_MISO = 37;
constexpr uint8_t PIN_SD_CS = 15;

constexpr uint8_t PIN_LORA_NSS = 8;
constexpr uint8_t PIN_LORA_DIO_1 = 14;
constexpr uint8_t PIN_LORA_RESET = 12;
constexpr uint8_t PIN_LORA_BUSY = 13;

constexpr uint8_t PIN_LORA_CLK = 9;
constexpr uint8_t PIN_LORA_MISO = 11;
constexpr uint8_t PIN_LORA_MOSI = 10;
constexpr uint8_t PIN_LORA_CS = PIN_LORA_NSS;

constexpr uint8_t PIN_DIN_BUTTON = GPIO_NUM_6;

namespace vext {
  void enable();
  void disable();
  void on();
  void off();
}

namespace button {
  bool pressed();
}

#endif
