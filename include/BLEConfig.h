#ifndef BLECONFIG_H
#define BLECONFIG_H

#include <SafeString.h>
#include <BLECharacteristic.h>
#include <BLEUUID.h>
#include <BLE2902.h>
#include <BLE2904.h>

#include "Utils.h"
#include "BLEControl.h"
#include "RTC.h"
#include "Battery.h"
#include "TaskFunctions.h"
#include "Storage.h"
#include "Defines.h"

const static BLEUUID SYSTEM_SERVICE_UUID("00000001-d913-4e77-844e-26088d4fef37");              // Handles: 1
const static BLEUUID SYSTEM_TIME_CHAR_UUID("00000002-276e-45a0-98c8-5c957b1102b9");            // Handles: 2
const static BLEUUID SYSTEM_BATTERY_CHAR_UUID("00000003-b12e-469a-8d88-cfa3724bee90");         // Handles: 2
const static BLEUUID SYSTEM_INTERVAL_CHAR_UUID("00000004-7392-4c6f-994b-f826b36f707e");        // Handles: 2
const static BLEUUID SYSTEM_TOKEN_CHAR_UUID("00000005-fc0b-4e0b-bed4-c2ff8dde785c");           // Handles: 2
const static BLEUUID SYSTEM_RESET_CHAR_UUID("00000006-da3b-4184-8fbf-6bed2ff99adb");           // Handles: 2
const static BLEUUID SYSTEM_FIRMWARE_VERSION_CHAR_UUID("00000007-832c-40bc-aa3c-2b7734ded777");// Handles: 2
const static BLEUUID SYSTEM_FIRMWARE_UPDATE_CHAR_UUID("00000008-11f1-4dcd-ae04-c73ed89e0c9d"); // Handles: 2 + 1

const static BLEUUID PAIR_SERVICE_UUID("00000001-1750-4be4-9b7b-ca810732ec93");                // Handles: 1
const static BLEUUID PAIR_BEGIN_CHAR_UUID("00000002-1223-43f9-8933-e67ebf03cad7");             // Handles: 2
const static BLEUUID PAIR_SENSOR_INFO_CHAR_UUID("00000003-65a2-426b-ad5c-96c8c5d9c18c");       // Handles: 2 + 1
const static BLEUUID PAIR_START_CYCLE_CHAR_UUID("00000004-1af1-4ae1-86d8-7bc15839cdbc");       // Handles: 2

const static BLEUUID WIFI_SERVICE_UUID("00000001-78b6-49a7-8176-f4b5f7531a8b");                // Handles: 1
const static BLEUUID WIFI_SSID_CHAR_UUID("00000002-0176-4a78-aca0-a0975e5bf760");              // Handles: 2
const static BLEUUID WIFI_PASSWORD_CHAR_UUID("00000004-dccb-4871-8f6d-b7fcbeb16890");          // Handles: 2
const static BLEUUID WIFI_SCAN_START_CHAR_UUID("00000005-e348-409b-9c40-f8dd89967c7d");        // Handles: 2
const static BLEUUID WIFI_SCAN_RESULTS_CHAR_UUID("00000006-89e0-40bf-9a3d-3dbe0c2bc306");      // Handles: 2 + 1

const static BLEUUID SERVER_SERVICE_UUID("00000001-d44e-4975-ae6b-e6acffbc5a58");              // Handles: 1
const static BLEUUID SERVER_API_DOMAIN_CHAR_UUID("00000002-73aa-4252-a4b7-5ee9e7e38b7b");      // Handles: 2
const static BLEUUID SERVER_STATIC_DOMAIN_CHAR_UUID("00000003-c78d-4b56-ab1d-ede67e495855");   // Handles: 2

const static BLEUUID TEST_SERVICE_UUID("00000001-2bb9-4751-a21b-544174e97c31");                // Handles: 1
const static BLEUUID TEST_COMMAND_CHAR_UUID("00000002-e43f-4b55-99a0-91e3af48fb81");           // Handles: 2
const static BLEUUID TEST_RESULT_CHAR_UUID("00000003-257b-4578-87f2-da88387a7e91");            // Handles: 2 + 1

const static BLEUUID READINGS_SERVICE_UUID("00000001-396e-4177-b9f1-324da78413b9");            // Handles: 1
const static BLEUUID READINGS_COMMAND_CHAR_UUID("00000002-20f0-4819-ac37-0dfc4f76555b");       // Handles: 2
const static BLEUUID READINGS_DATA_CHAR_UUID("00000003-d9b8-419d-832c-b1262683e7c0");          // Handles: 2 + 1

extern SafeString chipIDAscii;

extern RTC_DATA_ATTR uint64_t timeNTPUpdated;
extern ConfigState configState;

extern bool gIsPairing;

extern SafeString gToken;
extern uint8_t gInterval;
extern SafeString gFirmwareVersion;
extern SafeString gFirmwareUpdateVersion;
extern SafeString gWifiSsid;
extern SafeString gWifiPassword;
extern SafeString gAPIDomain;
extern SafeString gStaticDomain;
extern uint8_t gBattery;
extern TestResult gTestResult;
extern int32_t gBytesReceivedReadingTransfer;
extern uint16_t gMtu;
extern TestResult gTestResult;
extern bool gBleConnected;

extern void systemReset();
extern void updateFirmware(SafeString &firmwareUpdateUrl);
extern void pairBegin();
extern void pairStartCycle();

extern void startPowerOffTimer();
extern void stopPowerOffTimer();

void bleConfigSetup(SafeString &gatewayBLEName);
void bleSetTime(uint32_t time);
void bleSetBattery(uint16_t battery);
void bleSetInterval(uint8_t interval);
void bleSetFirmwareVersion(SafeString &firmwareVersion);
void bleSetFirmwareUpdateStatus(int8_t status, uint8_t progress);
void bleSetSensorInfo(SafeString &sensorInfo);
void bleSetWifiSsid(SafeString &ssid);
void bleSetWifiScanResults(SafeString &scanResult);
void bleSetAPIDomain(SafeString &domain);
void bleSetStaticDomain(SafeString &domain);
void bleSetTestResult(TestResult &res);

#endif
