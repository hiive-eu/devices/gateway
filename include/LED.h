#ifndef LED_H
#define LED_H

#include <Arduino.h>
#include "Hardware.h"

constexpr uint8_t CHANNEL_R = 2;
constexpr uint8_t CHANNEL_G = 3;
constexpr uint8_t CHANNEL_B = 4;

constexpr uint8_t RGB_RESOLUTION = 8;
constexpr uint16_t RGB_FREQ = 1000;

namespace led {
  void enable();
  void disable();
  void redOn(uint8_t brightness = 64);
  void redOff();
  void greenOn(uint8_t brightness = 64);
  void greenOff();
  void blueOn(uint8_t brightness = 64);
  void blueOff();
  void allOff();
  void yellow(uint8_t brightness = 64);
  void orange(uint8_t brightness = 64);
  void cyan(uint8_t brightness = 64);
}
#endif
