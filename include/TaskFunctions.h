#ifndef _TASKFUNCTIONS_H_
#define _TASKFUNCTIONS_H_

#include <Arduino.h>
#include <HTTPClient.h>
#include <BLECharacteristic.h>

#include "SDControl.h"
#include "WiFiControl.h"

#if defined(DEBUG) && DEBUG > 0
extern SafeString gDevAuthUser; 
extern SafeString gDevAuthPassword;
#endif
extern SafeString gToken;
extern SafeString gMacAscii;
extern SafeString gHTTPUserAgent;
extern SafeString gWifiSsid;
extern SafeString gAPIDomain;
extern SafeString gStaticDomain;
extern SafeString gCACert;
extern SafeString CACERT_PATH;
extern SafeString TEST_PATH;
extern uint16_t HTTP_TIMEOUT_MS;
extern int32_t gBytesReceivedReadingTransfer;
extern uint16_t gMtu;
extern uint8_t bootProgress;

struct WifiTestResult {
  int8_t res = -127;
  int8_t rssi = -127;
};

struct SDTestResult {
  int8_t res = -127;
  int8_t spaceUsed = -127;
};

struct TestResult {
  WifiTestResult wifi;
  SDTestResult sd;
};

extern TestResult gTestResult;

bool isValidWifiNetwork(wifi_auth_mode_t authMode);

void scanWifi(BLECharacteristic *pWifiScanResultsChar);
void testWifi(bool updateBLEChar);
void testSD(bool updateBLEChar);
void sendFilenames(BLECharacteristic *readingsChar);
void sendReading(SafeString &filename, BLECharacteristic *readingsChar);
void deleteReadingFile(SafeString &filename, BLECharacteristic *pReadingsTxChar);

void scanWifiTask(void *pvParameters);
void testSDTask(void *pvParameters);
void testWifiTask(void *pvParameters);
void sendFilenamesTask(void *pvParameters);
void sendReadingTask(void *pvParameters);
void deleteReadingFileTask(void *pvParameters);

#endif
