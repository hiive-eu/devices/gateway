#ifndef MISC_H
#define MISC_H

#include <SafeString.h>
#include "RTC.h"

constexpr float MS_TO_S = 0.001;
constexpr uint32_t S_TO_MS = 1000;
constexpr uint32_t S_TO_US = 1000000;
constexpr uint32_t M_TO_S = 60;

void copyBytesToAscii(SafeString &ascii, uint8_t *bytes, uint8_t size);
void copySensorMacBytesToAscii(SafeString &ascii, uint8_t *bytes);
void U64MacToBytes(uint8_t macBytes[6], uint64_t mac);
uint64_t u8BytesToU64(uint8_t u8[6]);
void printDate(DateTime t);
void printDate(uint64_t t);
#endif
