#ifndef MP2667_H
#define MP2667_H

#include <Arduino.h>
#include <Wire.h>

#include "Hardware.h"

constexpr uint8_t MP2667_REG_ADDR_INPUT_SRC_CTRL = 0x00;
constexpr uint8_t MP2667_REG_ADDR_POWER_ON_CONF = 0x01;
constexpr uint8_t MP2667_REG_ADDR_CHARGE_CURRENT_CTRL = 0x02;
constexpr uint8_t MP2667_REG_ADDR_DISCHARGE_TERMINATION_CURRENT_CTRL = 0x03;
constexpr uint8_t MP2667_REG_ADDR_CHARGE_VOLTAGE_CTRL = 0x04;
constexpr uint8_t MP2667_REG_ADDR_CHARGE_TERMINATION_TIMER_CTRL = 0x05;
constexpr uint8_t MP2667_REG_ADDR_MISC_OPS = 0x06;
constexpr uint8_t MP2667_REG_ADDR_SYSTEM_STATUS = 0x07;
constexpr uint8_t MP2667_REG_ADDR_FAULT = 0x08;

constexpr uint8_t MP2667_VAL_UVLO = 0x06;

enum MP2667ChargingStatus {
  NOT_CHARGING = 0,
  PRE_CHARGE = 1,
  CHARGING = 2,
  CHARGE_DONE = 3,
};

enum MP2667ChargingCurrent {
  CHARGE_CURRENT_26MA =   0b00000,
  CHARGE_CURRENT_59MA =   0b00001,
  CHARGE_CURRENT_125MA =  0b00011,
  CHARGE_CURRENT_257MA =  0b00111,
  CHARGE_CURRENT_521MA =  0b01111,
  CHARGE_CURRENT_1049MA = 0b11111,
};

enum MP2667ChargingVoltage {
  CHARGE_VOLTAGE_3600MV = 0b00000000,
  CHARGE_VOLTAGE_3615MV = 0b00000100,
  CHARGE_VOLTAGE_3645MV = 0b00001100,
  CHARGE_VOLTAGE_3705MV = 0b00011100,
  CHARGE_VOLTAGE_3825MV = 0b00111100,
  CHARGE_VOLTAGE_4065MV = 0b01111100,
  CHARGE_VOLTAGE_4545MV = 0b11111100,
};

enum MP2667UndervoltageLockout {
  UVLO_2400MV = 0b000,
  UVLO_2500MV = 0b001,
  UVLO_2700MV = 0b011,
  UVLO_3100MV = 0b111,
};

uint8_t replaceBits(uint8_t value, int n, int m, uint8_t new_bits);
uint8_t extractBits(uint8_t value, int n, int m);

class MP2667 {
  public:
    MP2667() {};
    void init();
    void reset();
    void enableShippingMode();
    void disableShippingMode();
    uint8_t getChargingCurrent();
    uint8_t getChargingVoltage();
    uint8_t getUndervoltageLockout();
    uint8_t getChargingStatus();
    uint8_t getFetDis();
    void setChargingCurrent(MP2667ChargingCurrent v);
    void setChargingVoltage(MP2667ChargingVoltage v);
    void setUndervoltageLockout(MP2667UndervoltageLockout v);
    void printAllRegs();
  private:
    uint8_t _readReg(uint8_t reg);
    void _writeReg(uint8_t reg, uint8_t v);
    uint8_t _readInputSourceControlReg();
    uint8_t _readPowerOnConfReg();
    uint8_t _readChargeCurrentControlReg();
    uint8_t _readDishargeTerminationCurrentControlReg();
    uint8_t _readChargeVoltageControlReg();
    uint8_t _readChargeTerminationTimerControlReg();
    uint8_t _readMiscOperationsReg();
    uint8_t _readSystemStatusReg();
    uint8_t _readFaultReg();
};

#endif

