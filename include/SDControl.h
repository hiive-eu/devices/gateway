#ifndef SDCONTROL_H
#define SDCONTROL_H

#include <Arduino.h>
#include <FS.h>
#include <SD.h>
#include <SPI.h>
#include <SafeString.h>

#include "Hardware.h"

namespace sd {
  extern bool enabled;
  bool enable();
  void disable();
  bool createFile(SafeString &filePath);
  bool removeFile(SafeString &filePath);
  bool createDir(SafeString &dirPath);
  bool removeDir(SafeString &dirPath);
  bool writeFile(SafeString &filePath, SafeString &content);
  bool readFile(SafeString &filePath, SafeString &buf);
  bool exists(SafeString &path);
  bool isDir(SafeString &path);
  int8_t spaceUsed();
}

#endif
