#ifndef WIFICONTROL_H
#define WIFICONTROL_H

#include <Arduino.h>
#include <SafeString.h>
#include <WiFi.h>

namespace wifi {
  extern bool enabled;

  bool enable(
    SafeString &ssid,
    SafeString &pass,
    void (*onLostConFunc)(WiFiEvent_t event, WiFiEventInfo_t info),
    void (*onGotIpFunc)(WiFiEvent_t event, WiFiEventInfo_t info)
  );
  bool enable(
    SafeString &ssid,
    SafeString &pass
  );
  void disable();
  void connect();
  void disconnect();
  bool isConnected();
  int8_t getRSSI();
}
#endif
