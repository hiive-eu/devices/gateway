#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#include <Arduino.h>

// PACKET SIZES
constexpr uint8_t HEARTBEAT_PAYLOAD_SIZE          =  8; // 1: packet code
                                                        // 6: sensorMac
                                                        // 1: battery

constexpr uint8_t START_CYCLE_PAYLOAD_SIZE        = 10; // 1: packet code
                                                        // 6: sensorMac
                                                        // 1: sleepInterval
                                                        // 1: minutesTillWakeup
                                                        // 1: secondsTillWakeup

constexpr uint8_t REQ_PAIR_PAYLOAD_SIZE           =  8; // 1: packet code
                                                        // 6: sensorMac
                                                        // 1: battery

constexpr uint8_t RESP_PAIR_PAYLOAD_SIZE          = 14; // 1: packet code
                                                        // 6: sensorMac
                                                        // 1: sensorNum 
                                                        // 6: gatewayMac

constexpr uint8_t REQ_READING_PAYLOAD_SIZE        = 40; //  1: packet code
                                                        //  6: sensorMac
                                                        //  2: battery
                                                        //  1: temperature
                                                        //  1: humidity
                                                        //  2: imu_x
                                                        //  2: imu_y
                                                        //  2: imu_z
                                                        // 20: frequencyBins
                                                        //  1: soundLevel
                                                        //  2: packetNum

constexpr uint8_t RESP_READING_PAYLOAD_SIZE       = 10; // 1: packet code
                                                        // 6: gatewayMac
                                                        // 1: sleepInterval
                                                        // 1: minutesTillWakeup
                                                        // 1: secondsTillWakeup

constexpr uint8_t BROADCAST_ID[6] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

enum MessageCode {
  // Sensor requests w.o. response
  HEARTBEAT_CODE = 0x01,

  // Gateway requests w.o. response
  START_CYCLE_CODE = 0x02,

  // Sensor requests with gateway responses
  REQ_PAIR_CODE = 0x10,
  RESP_PAIR_CODE = 0x20,

  REQ_READING_CODE = 0x11,
  RESP_READING_CODE = 0x21,
};

extern uint8_t gMacBytes[6];
extern void setLastSentMessage(MessageCode messageCode);

void sendPairResponse(uint8_t sensorBytes[6], uint8_t sensorNumber);
void sendReadingResponse(uint8_t interval, uint8_t minutes, uint8_t seconds);
void sendStartCycle(uint8_t interval, uint8_t minutesTillWakeup, uint8_t secondsTillWakeup);

#endif
