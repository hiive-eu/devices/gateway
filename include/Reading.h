#ifndef _READING_H_
#define _READING_H_

#include <Arduino.h>
#include <ArduinoJson.h>
#include <SafeString.h>

#include <MCP7940.h>

#include "BLEConfig.h"
#include "RTC.h"
#include "SDControl.h"

#include "Defines.h"
#include "Sensors.h"
#include "TaskFunctions.h"
#include "WiFiControl.h"

extern HTTPClient httpClient;
#if defined(DEBUG) && DEBUG > 0
extern SafeString gDevAuthUser; 
extern SafeString gDevAuthPassword;
#endif

extern SafeString gMacAscii;
extern SafeString gHTTPUserAgent;
extern SafeString gAPIDomain;
extern SafeString gStaticDomain;
extern SafeString gToken;
extern SafeString gCACert;
extern uint16_t HTTP_TIMEOUT_MS;

constexpr uint8_t NUM_OF_FREQUENCY_BINS = 20;
constexpr uint16_t READING_JSON_SIZE = 2048;

constexpr uint8_t DIRECTORY_PATH_SIZE = 9; // /readings
constexpr uint8_t READING_FILE_NAME_SIZE = 7; // yyyy-mm
constexpr uint8_t FILE_PATH_SIZE = DIRECTORY_PATH_SIZE + 1 + READING_FILE_NAME_SIZE; // 1 is "/"


extern SafeString READING_PATH;

struct __attribute__((__packed__)) SensorReading {
  uint64_t mac = 0;
  uint8_t bat;
  int8_t temp;
  uint8_t hum;
  int16_t accX;
  int16_t accY;
  int16_t accZ;
  uint8_t frequencyBins[NUM_OF_FREQUENCY_BINS] = {0};
  uint8_t soundLevel;
  int8_t rssi;
}; // size = 20

struct __attribute__((__packed__)) GatewayReading {
  uint8_t bat;
  int8_t temp;
  uint8_t hum;
  int16_t accX;
  int16_t accY;
  int16_t accZ;
}; // size = 9

struct SDInfo {
  int8_t status;
  uint8_t spaceUsed;
};

struct WifiInfo {
  int8_t rssi;
};

struct Reading {
  GatewayReading gatewayReading;
  SensorReading sensorReadings[MAX_SENSORS];
  SDInfo sdInfo;
  WifiInfo wifiInfo;
  DateTime time;
};

void readingToJson(Reading reading, SafeString &readingJsonString);
void readingToByteArray(Reading reading, uint8_t numReadings, uint8_t* outArray);
uint8_t storeReading(Reading reading);
int8_t listReadingFiles(SafeString &outReadingFiles);
File getReadingFileHandle(char filename[8]);
bool deleteReadingFile(char filename[8]);
int16_t sendReadingHTTP(SafeString &readingJsonString);
#endif
