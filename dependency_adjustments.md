Adjust the files and create a directory for it in `include/`.

Example structure:

```
include/:
  |-MC34x9/
    |-MC34x9.h
    |-MC34x9.cpp
```

# MCP7940

## MCP7940.h

Replace:

```
#include "Arduino.h"
#include "Wire.h"
```

with: 

```
#include <Arduino.h>
#include <Wire.h>
```

### MCP7940\_Class::begin()

Replace:

```
bool begin(const uint32_t i2cSpeed = I2C_STANDARD_MODE) const;
```

with:

```
bool begin() const;
```

## MCP7940.cpp

### MCP7940\_Class::begin()

Replace:

```
bool MCP7940_Class::begin(const uint32_t i2cSpeed) const {
```

with:

```
bool MCP7940_Class::begin() const {
```

Remove `Wire.begin()` and `Wire.setClock(i2cSpeed)` Should be called from main code.

# MC34x9

## MC34x9.h

### MC34X9

Define I2C address:

```
#define MC34X9_DEFAULT_ADDR 0x4c
```

Replace:

```
bool start(bool bSpi, uint8_t chip_select)
```

with:

```
bool start(bool bSpi)
```

## MC34x9.cpp

### m\_drv\_i2c\_init

Remove `Wire.begin()`. Should be called from main code.

### _readRegister8

Replace:

```
Wire.requestFrom(chip_select, 1, reg, 1, true);
value = Wire.read();
```

with:

```
Wire.beginTransmission(chip_select);                                        
Wire.write(reg);                                                            
Wire.endTransmission();                                                     
Wire.requestFrom(chip_select, 1, true);                                     
value = Wire.read();                                                        
Wire.endTransmission();
```

### start

Replace:

```
bool MC34X9::start(bool bSpi, uint8_t chip_select)
```

with:

```
bool MC34X9::start(bool bSpi)
{
  uint8_t chip_select = MC34X9_DEFAULT_ADDR;
```

# Adafruit\_I2CDevice

## Adafruit\_I2CDevice.cpp

### Adafruit\_I2CDevice::begin

Remove `_wire->begin()`. Should be called from main code.


