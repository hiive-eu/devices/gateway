import os
import time
import subprocess

Import("env")

def is_nixos():
    try:
        with open('/etc/os-release') as f:
            os_release = f.read()
        if 'ID=nixos' in os_release:
            return True
    except FileNotFoundError:
        pass
    return False

print("PRESCRIPT: erase_flash.py")

UPLOAD_PORT = env.get("UPLOAD_PORT")
if UPLOAD_PORT != None:
    command = ""
    if is_nixos():
        command = f"esptool.py --port {UPLOAD_PORT} --chip esp32-s3 erase_flash"
    else:
        command = f"python3 $(which esptool.py) --port {UPLOAD_PORT} --chip esp32-s3 erase_flash"

    p = subprocess.call(f"{command}", shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    time.sleep(2)
    
    print("Flash erased")
