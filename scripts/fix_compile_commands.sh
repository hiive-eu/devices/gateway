#!/usr/bin/env bash

sed -i 's/xtensa-esp32s3-elf-g++/xtensa-esp32s3-elf-gcc/g' compile_commands.json

sed -i 's/-mlongcalls/-mlong-calls/g' compile_commands.json
sed -i 's/-fstrict-volatile-bitfields//g' compile_commands.json
sed -i 's/-fno-tree-switch-conversion//g' compile_commands.json
